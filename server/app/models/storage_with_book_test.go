package models_test

import (
	"testing"

	"github.com/company1101/engel/server/app/utils"

	"github.com/company1101/engel/server/app/models"
)

// TODO: 各modelのコンストラクタのテスト
// NOTE: 本当はinterfaceをかませたいけどジェネリクスがないせいで
// 単純な実装ではどこかでしわ寄せがきそう…
var values = [][]interface{}{
	[]interface{}{"lunch11", "1/1", 1, 1},
	[]interface{}{"lunch12", "1/2", 2, 1},
	[]interface{}{"lunch13", "1/3", 3, 1},
	[]interface{}{"lunch14", "1/4", 4, 2},
	[]interface{}{"lunch15", "1/5", 5, 2},
	[]interface{}{"lunch16", "1/6", 1, 2},
	[]interface{}{"lunch17", "1/7", 2, 3},
	[]interface{}{"lunch18", "1/8", 3, 3},
	[]interface{}{"lunch19", "1/9", 4, 4},
	[]interface{}{"lunch21", "2/1", 5, 4},
}

func initTest(t *testing.T) []models.Book {
	t.Helper()
	var err error
	ptr := models.NewEmptyBook()
	err = models.RefreshDatabase(storage, ptr)
	if err != nil {
		utils.TestFailedWithError(t, err)
	}

	created, err := models.CreateBooks(storage, ptr, values)
	if err != nil {
		utils.TestFailedWithError(t, err)
	}
	return created
}

func TestCount(t *testing.T) {
	var err error
	_ = initTest(t)
	createdBy := 3
	numCreatedBy := 2
	ptr := models.NewEmptyBook()
	count, err := storage.Count(ptr, models.BookCreatedBy.String(), createdBy)

	if err != nil {
		utils.TestFailedWithError(t, err)
	}
	utils.AssertValueEqual(t, count, numCreatedBy)
}

func TestFetchOneByID(t *testing.T) {
	var err error
	created := initTest(t)
	idx := 2
	ptr := models.NewEmptyBook()
	err = storage.FetchOne(ptr, models.BookID.String()+"= ?", idx)
	if err != nil {
		utils.TestFailedWithError(t, err)
	}
	utils.AssertModelEqual(t, ptr, &created[idx-1])
}

func TestUpdateOne(t *testing.T) {
	var err error
	_ = initTest(t)
	idx := 2
	newTitle := "new title"
	newDescription := "new description"
	ptr := models.NewEmptyBook()
	newValues := models.NewBookMap(newTitle, newDescription)
	err = storage.UpdateOne(ptr, newValues, models.BookID.String()+" = ?", idx)
	if err != nil {
		utils.TestFailedWithError(t, err)
	}

	utils.AssertValueEqual(t, ptr.Title, newTitle)
	utils.AssertValueEqual(t, ptr.Description, newDescription)
}

func testFetchManyCore(
	t *testing.T,
	storage *models.Storage,
	orderBy string,
	first interface{},
	limit int,
	isAscending bool,
	field string,
	id int,
	answer []models.Book,
) {
	t.Helper()
	c := models.NewEmptyBooks()
	err := storage.FetchMany(&c, orderBy, first, limit, isAscending, field+" = ?", id)
	if err != nil {
		utils.TestFailedWithError(t, err)
	}
	utils.AssertLengthEqual(t, len(c), len(answer))
	for i, v := range c {
		utils.AssertModelEqual(t, &v, &answer[i])
	}

}
func TestFetchMany(t *testing.T) {
	created := initTest(t)
	testFetchManyCore(
		t, storage,
		models.BookCreatedAt.String(), created[1].CreatedAt, 2, false, models.BookCreatedBy.String(), 1,
		[]models.Book{created[1], created[0]},
	)
	testFetchManyCore(
		t, storage,
		models.BookCreatedAt.String(), created[1].CreatedAt, 2, true, models.BookCreatedBy.String(), 1,
		[]models.Book{created[1], created[2]},
	)
	testFetchManyCore(
		t, storage,
		models.BookCreatedAt.String(), nil, 2, true, models.BookCreatedBy.String(), 1,
		[]models.Book{created[0], created[1]},
	)
	testFetchManyCore(
		t, storage,
		models.BookCreatedAt.String(), nil, 5, true, models.BookCreatedBy.String(), 1,
		[]models.Book{created[0], created[1], created[2]},
	)
	testFetchManyCore(
		t, storage,
		models.BookCreatedAt.String(), nil, 2, false, models.BookCreatedBy.String(), 100, // out of range
		[]models.Book{},
	)
}
