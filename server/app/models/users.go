package models

import (
	"time"

	"github.com/company1101/engel/server/app/utils"
)

type UserColname string

const (
	UserID          UserColname = "id"
	UserAccountName UserColname = "account_name"
	UserDisplayName UserColname = "display_name"
	UserPhotoPath   UserColname = "photo_path"
	UserCreatedAt   UserColname = GORM_CREATED_AT
	UserDeletedAt   UserColname = GORM_DELETED_AT
)

func (c UserColname) String() string {
	return string(c)
}

func (c UserColname) Validate() error {
	if !utils.ContainStr(
		[]string{
			string(UserID),
			string(UserAccountName),
			string(UserDisplayName),
			string(UserPhotoPath),
			string(UserCreatedAt),
			string(UserDeletedAt),
		},
		string(c),
	) {
		return &utils.EnumNotContained{Value: string(c)}
	}
	return nil
}

type User struct {
	ID          int    `gorm:"primary_key"`
	AccountName string `gorm:"size:16;unique_index"`
	DisplayName string `gorm:"size:16"`
	PhotoPath   string `gorm:"size:256"`
	CreatedAt   time.Time
	DeletedAt   *time.Time
}

func NewEmptyUser() *User {
	return &User{}
}

func NewEmptyUsers() []User {
	var objs []User
	return objs
}

func NewUser(accountName, displayName, photoPath string) *User {
	return &User{
		AccountName: accountName,
		DisplayName: displayName,
		PhotoPath:   photoPath,
	}
}

func NewUserMap(accountName, displayName, photoPath string) map[string]interface{} {
	return map[string]interface{}{
		UserAccountName.String(): accountName,
		UserDisplayName.String(): displayName,
		UserPhotoPath.String():   photoPath,
	}
}
