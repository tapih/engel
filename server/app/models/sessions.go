package models

import (
	"time"

	"github.com/company1101/engel/server/app/utils"
)

type SessionColname string

const (
	SessionID        SessionColname = "id"
	SessionUserID    SessionColname = "user_id"
	SessionToken     SessionColname = "token"
	SessionCreatedAt SessionColname = GORM_CREATED_AT
)

func (c SessionColname) String() string {
	return string(c)
}

func (c SessionColname) Validate() error {
	if !utils.ContainStr(
		[]string{
			string(SessionID),
			string(SessionUserID),
			string(SessionToken),
			string(SessionCreatedAt),
		},
		string(c),
	) {
		return &utils.EnumNotContained{Value: string(c)}
	}
	return nil
}

type Session struct {
	ID        int    `gorm:"primary_key"`
	Token     string `gorm:"size:256;unique_index"`
	UserID    int    `gorm:"unique"`
	CreatedAt time.Time
}

func NewEmptySession() *Session {
	return &Session{}
}

func NewSession(userID int) *Session {
	return &Session{
		UserID: userID,
		Token:  utils.MakeUniqString(),
	}
}
