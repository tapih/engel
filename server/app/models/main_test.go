package models_test

import (
	"os"
	"testing"

	"github.com/company1101/engel/server/app/configs"
	"github.com/company1101/engel/server/app/models"
	_ "github.com/lib/pq"
)

var storage *models.Storage

func TestMain(m *testing.M) {
	var err error

	args := configs.ParseArgsForTest()

	path := args.GetConfigPath()

	config, err := configs.LoadConfig(path)
	if err != nil {
		panic(err)
	}

	storage, err = config.DB.NewStorage()
	if err != nil {
		panic(err)
	}

	// run test here
	ret := m.Run()
	defer os.Exit(ret)

	err = models.DropTablesIfExists(storage)
	if err != nil {
		panic(err)
	}
}
