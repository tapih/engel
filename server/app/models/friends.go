package models

import (
	"time"

	"github.com/company1101/engel/server/app/utils"
)

type FriendColname string

const (
	FriendID          FriendColname = "id"
	FriendRequestedBy FriendColname = "requested_by"
	FriendRequestedTo FriendColname = "requested_to"
	FriendIsAccepted  FriendColname = "is_acceptted"
	FriendCreatedAt   FriendColname = GORM_CREATED_AT
	FriendDeletedAt   FriendColname = GORM_DELETED_AT
)

func (c FriendColname) String() string {
	return string(c)
}

func (c FriendColname) Validate() error {
	if !utils.ContainStr(
		[]string{
			string(FriendID),
			string(FriendRequestedBy),
			string(FriendRequestedTo),
			string(FriendIsAccepted),
			string(FriendCreatedAt),
			string(FriendDeletedAt),
		},
		string(c),
	) {
		return &utils.EnumNotContained{Value: string(c)}
	}
	return nil
}

type Friend struct {
	ID          int  `gorm:"primary_key"`
	RequestedBy int  `gorm:"not null"` // => user_id
	RequestedTo int  `gorm:"not null"` // => user_id
	IsAccepted  bool `gorm:"default:'false'"`
	CreatedAt   time.Time
	DeletedAt   *time.Time
}

func NewEmptyFriend() *Friend {
	return &Friend{}
}

func NewEmptyFriends() []Friend {
	var objs []Friend
	return objs
}

func NewFriend(requestedBy int, requestedTo int) *Friend {
	return &Friend{
		RequestedBy: requestedBy,
		RequestedTo: requestedTo,
		IsAccepted:  false,
	}
}

func NewFriendMap() map[string]interface{} {
	return map[string]interface{}{FriendIsAccepted.String(): true}
}
