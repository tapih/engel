package models

import (
	"fmt"
	"time"

	"github.com/jinzhu/gorm"
)

func isNotFoundError(err error) bool {
	return err == gorm.ErrRecordNotFound
}

const (
	GORM_ID         = "id"
	GORM_CREATED_AT = "created_at"
	GORM_DELETED_AT = "deleted_at"
	GORM_UPDATED_AT = "updated_at"
)

type Storage struct {
	DB *gorm.DB
}

func NewStorage(
	dialect string,
	host string,
	port int,
	dbname string,
	user string,
	password string,
	timezone string,
	withSSL bool,
) (*Storage, error) {
	var err error

	sslmode := "disable"
	if withSSL {
		sslmode = "enable"
	}
	cnx := fmt.Sprintf(
		"host=%s port=%d dbname=%s user=%s password=%s timezone=%s sslmode=%s",
		host,
		port,
		dbname,
		user,
		password,
		timezone,
		sslmode,
	)

	db, err := gorm.Open(dialect, cnx)
	if err != nil {
		return nil, err
	}
	return &Storage{DB: db}, nil
}

// NOTE: genericsが使えるようになったら、new関数作ってテンプレートクラスにするんだ…

func (c *Storage) CreateTableIfNotExists(ptr interface{}) error {
	if !c.DB.HasTable(ptr) {
		if err := c.DB.CreateTable(ptr).Error; err != nil {
			return err
		}
	}
	return nil
}

func (c *Storage) DropTableIfExists(ptr interface{}) error {
	if err := c.DB.DropTableIfExists(ptr).Error; err != nil {
		return err
	}
	return nil
}

func (c *Storage) Create(ptr interface{}) error {
	if err := c.DB.Create(ptr).Error; err != nil {
		return err
	}
	return nil
}

func (c *Storage) FetchAll(objsPtr interface{}) error {
	if err := c.DB.Find(objsPtr).Error; err != nil {
		return err
	}
	return nil
}

func (c *Storage) Delete(ptr interface{}, conditions string, values ...interface{}) error {
	if err := c.DB.Where(conditions, values...).Delete(ptr).Error; err != nil {
		return err
	}
	return nil
}

func (c *Storage) DeleteByIDs(ptr interface{}, field string, values []interface{}) error {
	for _, v := range values {
		err := c.Delete(ptr, field, v)
		if err != nil {
			return err
		}
	}
	return nil
}

func (c *Storage) DeleteExpires(ptr interface{}, time time.Time) error {
	if err := c.DB.Where(GORM_CREATED_AT+" < ?", time).Delete(ptr).Error; err != nil {
		return err
	}
	return nil
}

func (c *Storage) DeleteSoftDeleted(ptr interface{}) error {
	if err := c.DB.Unscoped().Where(GORM_DELETED_AT+" <> ?", nil).Delete(ptr).Error; err != nil {
		return err
	}
	return nil
}

func (c *Storage) FetchOne(ptr interface{}, conditions string, values ...interface{}) error {
	query := c.DB.Where(conditions, values...).First(ptr)
	if query.RecordNotFound() {
		return nil
	}
	if err := query.Error; err != nil {
		return err
	}
	return nil
}

func (c *Storage) Count(ptr interface{}, field string, value interface{}) (int, error) {
	var count int
	if err := c.DB.Model(ptr).Where(field+" = ?", value).Count(&count).Error; err != nil {
		return 0, err
	}
	return count, nil
}

func (c *Storage) UpdateOne(
	ptr interface{},
	newValueMap map[string]interface{},
	conditions string,
	values ...interface{},
) error {
	if err := c.DB.Model(ptr).Where(conditions, values...).Updates(newValueMap).Error; err != nil {
		return err
	}
	return nil
}

func (c *Storage) fetchManyCore(
	objsPtr interface{},
	orderBy string,
	first interface{},
	limit int,
	isAscending bool,
	conditions string,
	values ...interface{},
) error {
	query := c.DB
	if first != nil {
		op := " <= ?"
		if isAscending {
			op = " >= ?"
		}
		query = query.Where(orderBy+op, first)
	}
	query = query.Where(conditions, values...)

	order := "desc"
	if isAscending {
		order = "asc"
	}
	query = query.Order(fmt.Sprintf("%s %s, id asc", orderBy, order)).Limit(limit).Find(objsPtr)
	if err := query.Error; err != nil {
		return err
	}
	return nil
}

// TODO: validate colname (need generics)
func (c *Storage) FetchMany(
	objsPtr interface{},
	orderBy string,
	first interface{},
	limit int,
	isAscending bool,
	conditions string,
	values ...interface{},
) error {
	if err := c.fetchManyCore(
		objsPtr,
		orderBy,
		first,
		limit,
		isAscending,
		conditions,
		values...,
	); err != nil {
		return err
	}
	return nil
}

func (c *Storage) WithTransaction() *Storage {
	tx := c.DB.Begin()
	return &Storage{DB: tx}
}

func (c *Storage) Rollback() {
	c.DB.Rollback()
}

func (c *Storage) Commit() error {
	return c.DB.Commit().Error
}
