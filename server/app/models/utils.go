package models

import (
	"time"
)

func sum(arr []int) int {
	buf := 0
	for _, v := range arr {
		buf += v
	}
	return buf
}

func newModels() []interface{} {
	return []interface{}{
		NewEmptyBook(),
		NewEmptyClearanceRequest(),
		NewEmptyCurrency(),
		NewEmptyFriend(),
		NewEmptyItem(),
		NewEmptyMember(),
		NewEmptyPayment(),
		NewEmptySession(),
		NewEmptyUser(),
		NewEmptyOAuthAccount(OAuthProviderLine),
	}
}

func CreateTablesIfNotExists(storage *Storage) error {
	for _, ptr := range newModels() {
		if err := storage.CreateTableIfNotExists(ptr); err != nil {
			return err
		}
	}
	return nil
}

func DropTablesIfExists(storage *Storage) error {
	for _, ptr := range newModels() {
		if err := storage.DropTableIfExists(ptr); err != nil {
			return err
		}
	}
	return nil
}

func RefreshDatabase(storage *Storage, ptr interface{}) error {
	var err error
	err = storage.DropTableIfExists(ptr)
	if err != nil {
		return err
	}
	err = storage.CreateTableIfNotExists(ptr)
	if err != nil {
		return err
	}
	return nil
}

func CreateBooks(storage *Storage, ptr interface{}, values [][]interface{}) ([]Book, error) {
	var created []Book
	for _, v := range values {
		ptr := NewBook(v[0].(string), v[1].(string), v[2].(int), v[3].(int))
		err := storage.Create(ptr)
		time.Sleep(10 * time.Millisecond)
		if err != nil {
			return nil, err
		}
		created = append(created, *ptr)
	}
	return created, nil
}

func CreateItems(storage *Storage, ptr interface{}, values [][]interface{}) ([]Item, error) {
	var created []Item
	for _, v := range values {
		ptr := NewItem(v[0].(int), v[1].(string), v[2].(string), v[3].(string), v[4].(int))
		err := storage.Create(ptr)
		time.Sleep(10 * time.Millisecond)
		if err != nil {
			return nil, err
		}
		created = append(created, *ptr)
	}
	return created, nil
}
