package models

import (
	"time"

	"github.com/company1101/engel/server/app/utils"
)

type OAuthProviderName string

const (
	OAuthProviderLine OAuthProviderName = "line"
)

func (c OAuthProviderName) String() string {
	return string(c)
}

func (c OAuthProviderName) Validate() error {
	if !utils.ContainStr(
		[]string{
			string(OAuthProviderLine),
		},
		string(c),
	) {
		return &utils.EnumNotContained{Value: string(c)}
	}
	return nil
}

type OAuthColname string

const (
	OAuthID        OAuthColname = "id"
	OAuthToken     OAuthColname = "token"
	OAuthUserID    OAuthColname = "user_id"
	OAuthCreatedAt OAuthColname = GORM_CREATED_AT
	OAuthDeletedAt OAuthColname = GORM_DELETED_AT
)

func (c OAuthColname) String() string {
	return string(c)
}

func (c OAuthColname) Validate() error {
	if !utils.ContainStr(
		[]string{
			string(OAuthID),
			string(OAuthToken),
			string(OAuthUserID),
			string(OAuthCreatedAt),
			string(OAuthDeletedAt),
		},
		string(c),
	) {
		return &utils.EnumNotContained{Value: string(c)}
	}
	return nil
}

type LineAccount struct {
	ID        int    `gorm:"primary_key"`
	AccountID string `gorm:"size:64;unique_index"`
	UserID    int
	CreatedAt time.Time
	DeletedAt *time.Time
}

func NewEmptyOAuthAccount(provider OAuthProviderName) interface{} {
	return &LineAccount{}
}

func NewEmptyOAuthAccounts(provider OAuthProviderName) interface{} {
	var objs []LineAccount
	return objs
}

func NewOAuthAccount(provider OAuthProviderName, uniqString string) interface{} {
	return &LineAccount{AccountID: uniqString}
}

func NewOAuthAccountMap(userID string) map[string]interface{} {
	return map[string]interface{}{OAuthUserID.String(): userID}
}

//
