package models

import (
	"github.com/company1101/engel/server/app/utils"
	_ "github.com/revel/modules/orm/gorm"
)

type CurrencyColname string

const (
	CurrencyID   CurrencyColname = "id"
	CurrencyCode CurrencyColname = "code"
	CurrencyName CurrencyColname = "name"
)

func (c CurrencyColname) String() string {
	return string(c)
}

func (c CurrencyColname) Validate() error {
	if !utils.ContainStr(
		[]string{
			string(CurrencyID),
			string(CurrencyCode),
			string(CurrencyName),
		},
		string(c),
	) {
		return &utils.EnumNotContained{Value: string(c)}
	}
	return nil
}

type Currency struct {
	ID   int    `gorm:"primary_key"`
	Code string `gorm:"size:3;not null"`
	Name string `gorm:"size:16;not null"`
}

func NewEmptyCurrency() *Currency {
	return &Currency{}
}

func NewCurrency(code string, name string) *Currency {
	return &Currency{
		Code: code,
		Name: name,
	}
}

var Currencies = []Currency{*NewCurrency("JPY", "円")}

func CreateAllCurrencies(storage *Storage) ([]Currency, error) {
	var objs []Currency
	for _, obj := range Currencies {
		err := storage.Create(&obj)
		if err != nil {
			return nil, err
		}
		objs = append(objs, obj)
	}
	return objs, nil
}

func FetchAllCurrencies(storage *Storage) ([]Currency, error) {
	var objs []Currency
	err := storage.FetchAll(&objs)
	if err != nil {
		return nil, err
	}
	return objs, nil
}
