package models

import (
	"time"

	"github.com/company1101/engel/server/app/utils"
)

type ItemColname string

const (
	ItemID          ItemColname = "id"
	ItemBookID      ItemColname = "book_id"
	ItemTitle       ItemColname = "title"
	ItemDescription ItemColname = "description"
	ItemPhotoPath   ItemColname = "photo_path"
	ItemCreatedBy   ItemColname = "created_by"
	ItemCreatedAt   ItemColname = GORM_CREATED_AT
	ItemDeletedAt   ItemColname = GORM_DELETED_AT
)

func (c ItemColname) String() string {
	return string(c)
}

func (c ItemColname) Validate() error {
	if !utils.ContainStr(
		[]string{
			string(FriendID),
			string(FriendRequestedBy),
			string(FriendRequestedTo),
			string(FriendIsAccepted),
			string(FriendCreatedAt),
			string(FriendDeletedAt),
		},
		string(c),
	) {
		return &utils.EnumNotContained{Value: string(c)}
	}
	return nil
}

type Item struct {
	ID          int    `gorm:"primary_key"`
	BookID      int    `gorm:"not null"` // => book_id
	Title       string `gorm:"size:64;not null"`
	Description string `gorm:"size:256"`
	PhotoPath   string `gorm:"size:256"`
	CreatedBy   int    `gorm:"not null"` // => user_id
	CreatedAt   time.Time
	DeletedAt   *time.Time
}

func NewEmptyItem() *Item {
	return &Item{}
}

func NewEmptyItems() []Item {
	var objs []Item
	return objs
}

func NewItem(bookID int, title, description, photoPath string, createdBy int) *Item {
	return &Item{
		BookID:      bookID,
		Title:       title,
		Description: description,
		PhotoPath:   photoPath,
		CreatedBy:   createdBy,
	}
}

func NewItemMap(title, description, photoPath string) map[string]interface{} {
	return map[string]interface{}{
		ItemTitle.String():       title,
		ItemDescription.String(): description,
		ItemPhotoPath.String():   photoPath,
	}
}
