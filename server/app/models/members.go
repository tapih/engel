package models

import (
	"time"

	"github.com/company1101/engel/server/app/utils"
	"github.com/jinzhu/gorm"
)

type MemberColname string

const (
	MemberID        MemberColname = "id"
	MemberBookID    MemberColname = "book_id"
	MemberUserID    MemberColname = "user_id"
	MemberCreatedAt MemberColname = GORM_CREATED_AT
	MemberDeletedAt MemberColname = GORM_DELETED_AT
)

func (c MemberColname) String() string {
	return string(c)
}

func (c MemberColname) Validate() error {
	if !utils.ContainStr(
		[]string{
			string(MemberID),
			string(MemberBookID),
			string(MemberUserID),
			string(MemberCreatedAt),
			string(MemberDeletedAt),
		},
		string(c),
	) {
		return &utils.EnumNotContained{Value: string(c)}
	}
	return nil
}

type Member struct {
	ID        int `gorm:"primary_key"`
	BookID    int `gorm:"not null"` // => book_id
	UserID    int `gorm:"not null"` // => user_id
	CreatedAt time.Time
	DeletedAt *time.Time
}

type MemberStorage struct {
	DB *gorm.DB
}

func NewEmptyMember() *Member {
	return &Member{}
}

func NewEmptyMembers() []Member {
	var objs []Member
	return objs
}

func NewMember(bookID, userID int) *Member {
	return &Member{
		BookID: bookID,
		UserID: userID,
	}
}
