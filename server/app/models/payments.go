package models

import (
	"time"

	"github.com/company1101/engel/server/app/utils"
)

type PaymentColname string

const (
	PaymentID          PaymentColname = "id"
	PaymentItemID      PaymentColname = "item_id"
	PaymentUserID      PaymentColname = "user_id"
	PaymentAmountPaid  PaymentColname = "amount_paid"
	PaymentAmountToPay PaymentColname = "amount_to_pay"
	PaymentCreatedAt   PaymentColname = GORM_CREATED_AT
	PaymentDeletedAt   PaymentColname = GORM_DELETED_AT
)

func (c PaymentColname) String() string {
	return string(c)
}

func (c PaymentColname) Validate() error {
	if !utils.ContainStr(
		[]string{
			string(PaymentID),
			string(PaymentID),
			string(PaymentUserID),
			string(PaymentAmountPaid),
			string(PaymentAmountToPay),
			string(PaymentCreatedAt),
			string(PaymentDeletedAt),
		},
		string(c),
	) {
		return &utils.EnumNotContained{Value: string(c)}
	}
	return nil
}

type Payment struct {
	ID          int `gorm:"primary_key"`
	ItemID      int `gorm:"not null"` // => book
	UserID      int `gorm:"not null"` // => user
	AmountPaid  int `gorm:"not null"`
	AmountToPay int `gorm:"not null"`
	CreatedAt   time.Time
	DeletedAt   *time.Time
}

func NewEmptyPayment() *Payment {
	return &Payment{}
}

func NewEmptyPayments() []Payment {
	var objs []Payment
	return objs
}

func NewPayment(itemID, userID int, amountPaid, amountToPay int) *Payment {
	return &Payment{
		ItemID:      itemID,
		UserID:      userID,
		AmountPaid:  amountPaid,
		AmountToPay: amountToPay,
	}
}

func NewPaymentMap(amountPaid, amountToPay int) map[string]interface{} {
	return map[string]interface{}{
		PaymentAmountPaid.String():  amountPaid,
		PaymentAmountToPay.String(): amountToPay,
	}
}
