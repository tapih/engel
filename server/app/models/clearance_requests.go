package models

import (
	"time"

	"github.com/company1101/engel/server/app/utils"
)

type ClearanceRequestColname string

const (
	ClearanceRequestID          ClearanceRequestColname = "id"
	ClearanceRequestBookID      ClearanceRequestColname = "book_id"
	ClearanceRequestUserID      ClearanceRequestColname = "user_id"
	ClearanceRequestIsConfirmed ClearanceRequestColname = "is_confirmed"
	ClearanceRequestCreatedAt   ClearanceRequestColname = GORM_CREATED_AT
)

func (c ClearanceRequestColname) String() string {
	return string(c)
}

func (c ClearanceRequestColname) Validate() error {
	if !utils.ContainStr(
		[]string{
			string(ClearanceRequestID),
			string(ClearanceRequestBookID),
			string(ClearanceRequestUserID),
			string(ClearanceRequestIsConfirmed),
			string(ClearanceRequestCreatedAt),
		},
		string(c),
	) {
		return &utils.EnumNotContained{Value: string(c)}
	}
	return nil
}

type ClearanceRequest struct {
	ID          int  `gorm:"primary_key"`
	BookID      int  `gorm:"not null"` // => book_id
	UserID      int  `gorm:"not null"`
	IsConfirmed bool `gorm:"not null"`
	CreatedAt   time.Time
}

func NewEmptyClearanceRequest() *ClearanceRequest {
	return &ClearanceRequest{}
}

func NewEmptyClearanceRequests() []ClearanceRequest {
	var objs []ClearanceRequest
	return objs
}

func NewClearanceRequest(
	bookID int,
	userId int,
) *ClearanceRequest {
	return &ClearanceRequest{
		BookID:      bookID,
		UserID:      userId,
		IsConfirmed: false,
	}
}

func NewClearanceRequestMap(isConfirmed bool) map[string]interface{} {
	return map[string]interface{}{ClearanceRequestIsConfirmed.String(): isConfirmed}
}
