package models

import (
	"time"

	"github.com/company1101/engel/server/app/utils"
)

type BookColname string

const (
	BookID          BookColname = "id"
	BookTitle       BookColname = "title"
	BookDescription BookColname = "description"
	BookCurrencyID  BookColname = "currency_id"
	BookCreatedBy   BookColname = "created_by"
	BookCreatedAt   BookColname = GORM_CREATED_AT
	BookUpdatedAt   BookColname = GORM_UPDATED_AT
	BookDeletedAt   BookColname = GORM_DELETED_AT
)

func (c BookColname) String() string {
	return string(c)
}

func (c BookColname) Validate() error {
	if !utils.ContainStr(
		[]string{
			string(BookID),
			string(BookTitle),
			string(BookDescription),
			string(BookCurrencyID),
			string(BookCreatedBy),
			string(BookCreatedAt),
			string(BookDeletedAt),
		},
		string(c),
	) {
		return &utils.EnumNotContained{Value: string(c)}
	}
	return nil
}

type Book struct {
	ID          int    `gorm:"primary_key"`
	Title       string `gorm:"size:64;not null"`
	Description string `gorm:"size:256"`
	CurrencyID  int    `gorm:"not null"`
	CreatedBy   int    `gorm:"not null"` // => user_id
	CreatedAt   time.Time
	UpdatedAt   time.Time
	DeletedAt   *time.Time
}

func NewEmptyBook() *Book {
	return &Book{}
}

func NewEmptyBooks() []Book {
	var objs []Book
	return objs
}

func NewBook(
	title string,
	description string,
	currencyID int,
	createdBy int,
) *Book {
	return &Book{
		Title:       title,
		Description: description,
		CurrencyID:  currencyID,
		CreatedBy:   createdBy,
	}
}

func NewBookMap(title string, description string) map[string]interface{} {
	return map[string]interface{}{
		BookTitle.String():       title,
		BookDescription.String(): description,
	}
}
