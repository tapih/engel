package configs

import (
	"github.com/company1101/engel/server/app/controllers"
)

type Http struct {
	Port         int
	ReadTimeout  int    `yaml:"read_timeout"`
	WriteTimeout int    `yaml:"write_timeout"`
	WithSSL      bool   `yaml:"with_ssl"`
	SSLCert      string `yaml:"sslcert"`
	SSLKey       string `yaml:"sslkey"`
}

func (c *Http) NewRunner() *controllers.Runner {
	return controllers.NewRunner(
		c.Port,
		c.ReadTimeout,
		c.WriteTimeout,
		c.WithSSL,
		c.SSLCert,
		c.SSLKey,
	)
}
