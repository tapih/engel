package configs

import (
	"github.com/company1101/engel/server/app/middlewares"
	"github.com/gin-gonic/gin"
)

type Log struct {
	Path string
}

func (c *Log) NewMiddleware() gin.HandlerFunc {
	return middlewares.Logger(c.Path)
}
