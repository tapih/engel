package configs

import "github.com/company1101/engel/server/app/services"

type OAuth struct {
	ClientID      string `yaml:"client_id"`
	ClientSecret  string `yaml:"client_secret"`
	AuthEndpoint  string `yaml:"auth_endpoint"`
	TokenEndpoint string `yaml:"token_endpoint"`
	APIEndpoint   string `yaml:"api_endpoint"`
	RedirectURL   string `yaml:"redirect_url"`
	Scopes        []string
}

func (c *OAuth) NewHandler(name string) *services.OAuth {
	return services.NewOAuth(
		name,
		c.ClientID,
		c.ClientSecret,
		c.AuthEndpoint,
		c.TokenEndpoint,
		c.APIEndpoint,
		c.RedirectURL,
		c.Scopes,
	)
}
