package configs

import (
	"github.com/company1101/engel/server/app/middlewares"
	"github.com/gin-gonic/gin"
)

type Session struct {
	Name     string
	Path     string
	Domain   string
	MaxAge   int
	HttpOnly bool
	Secure   bool
	Secret   string
}

func (c *Session) NewMiddleware() gin.HandlerFunc {
	return middlewares.Session(
		c.Name,
		c.Path,
		c.Domain,
		c.MaxAge,
		c.Secure,
		c.HttpOnly,
		c.Secret,
	)
}
