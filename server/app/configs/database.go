package configs

import "github.com/company1101/engel/server/app/models"

type DB struct {
	Dialect  string
	Host     string
	Port     int
	Name     string
	User     string
	Password string
	Timezone string
	WithSSL  bool `yaml:"with_ssl"`
}

func (c *DB) NewStorage() (*models.Storage, error) {
	return models.NewStorage(
		c.Dialect,
		c.Host,
		c.Port,
		c.Name,
		c.User,
		c.Password,
		c.Timezone,
		c.WithSSL,
	)
}
