package configs

import (
	"flag"
	"fmt"
	"os"
	"path/filepath"
)

func pathJoin(a, b string) string {
	return filepath.Join(a, b)
}

const (
	MODE_PRODUCTION  = "prod"
	MODE_DEVELOPMENT = "devel"
	MODE_TESTING     = "test"
)

var DEFAULT_CONFIG_DIR = pathJoin(
	os.Getenv("GOPATH"),
	"src/github.com/company1101/engel/server/config/",
)

type Args struct {
	Mode       string
	ConfigDir  string
	DropTables bool
}

func ParseArgs() (*Args, error) {
	mode := flag.String("mode",
		MODE_DEVELOPMENT,
		fmt.Sprintf("run mode: \"%s\" or \"%s\" (default: %s)",
			MODE_PRODUCTION,
			MODE_DEVELOPMENT,
			MODE_DEVELOPMENT,
		),
	)
	configDir := flag.String("configDir", DEFAULT_CONFIG_DIR, "parent directory of config yaml")
	dropTables := flag.Bool("dropTables", false, "drop table or not (only in devel mode)")
	flag.Parse()

	args := Args{
		Mode:       *mode,
		ConfigDir:  *configDir,
		DropTables: *dropTables,
	}
	if !args.IsProd() && !args.IsDevel() {
		return nil, fmt.Errorf("invalid run mode: " + *mode)
	}
	return &args, nil
}

func (c *Args) IsProd() bool {
	return c.Mode == MODE_PRODUCTION
}

func (c *Args) IsDevel() bool {
	return c.Mode == MODE_DEVELOPMENT
}

func ParseArgsForTest() *Args {
	configDir := flag.String("configDir", DEFAULT_CONFIG_DIR, "parent directory of config yaml")
	flag.Parse()

	args := Args{
		Mode:       MODE_TESTING,
		ConfigDir:  *configDir,
		DropTables: false,
	}
	return &args
}

func (c *Args) GetConfigPath() string {
	return pathJoin(c.ConfigDir, c.Mode+".yml")
}
