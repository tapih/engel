package configs

import (
	"io/ioutil"

	"github.com/company1101/engel/server/app/models"
	yaml "gopkg.in/yaml.v2"
)

type Config struct {
	Name    string
	Http    Http
	Session Session
	DB      DB
	Log     Log
	OAuth   map[string]OAuth
}

func LoadConfig(path string) (*Config, error) {
	var err error
	var config Config

	buf, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}

	err = yaml.Unmarshal(buf, &config)
	if err != nil {
		return nil, err
	}

	for k, _ := range config.OAuth {
		if err = models.OAuthProviderName(k).Validate(); err != nil {
			return nil, err
		}
	}
	if err != nil {
		return nil, err
	}

	return &config, nil
}
