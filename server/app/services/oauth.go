package services

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/company1101/engel/server/app/utils"
	"golang.org/x/oauth2"
)

type OAuth struct {
	Name        string
	apiEndpoint string
	config      *oauth2.Config
}

func NewOAuth(
	name string,
	clientID string,
	clientSecret string,
	authEndpoint, tokenEndpoint, apiEndpoint string,
	redirectURL string,
	scopes []string,
) *OAuth {
	return &OAuth{
		Name:        name,
		apiEndpoint: apiEndpoint,
		config: &oauth2.Config{
			ClientID:     clientID,
			ClientSecret: clientSecret,
			Endpoint: oauth2.Endpoint{
				AuthURL:  authEndpoint,
				TokenURL: tokenEndpoint,
			},
			Scopes:      scopes,
			RedirectURL: redirectURL,
		}}
}

func (c *OAuth) MakeAuthURL() string {
	uniq := utils.MakeUniqString()
	return c.config.AuthCodeURL(uniq)
}

func (c *OAuth) FetchProfile(authCode string) (map[string]string, error) {
	body := make(map[string]string)

	ctx := context.Background()
	tok, err := c.config.Exchange(ctx, authCode)
	if err != nil {
		return body, err
	}

	cnn := c.config.Client(ctx, tok)
	resp, err := cnn.Get(c.apiEndpoint)
	if err != nil {
		return body, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return body, fmt.Errorf(resp.Status + " is returned by oauth host")
	}
	err = json.NewDecoder(resp.Body).Decode(&body)
	if err != nil {
		return body, err
	}
	return body, nil
}
