package controllers

import (
	"net/http"
	"strconv"

	"github.com/company1101/engel/server/app/utils"

	"github.com/company1101/engel/server/app/middlewares"
	"github.com/company1101/engel/server/app/models"
	"github.com/gin-gonic/gin"
)

func BookGet(storage *models.Storage, limit int) gin.HandlerFunc {
	return func(c *gin.Context) {
		// parse query
		var err error
		bookID, err := strconv.Atoi(c.Param(middlewares.PARAM_BOOK))
		if err != nil {
			c.JSON(
				http.StatusBadRequest,
				gin.H{RESPONSE_MESSAGE: err.Error()},
			)
			return
		}

		orderBy, first, isAscending, err := parsePaginationParams(c)
		if err != nil {
			c.JSON(
				http.StatusBadRequest,
				gin.H{RESPONSE_MESSAGE: err.Error()},
			)
			return
		}

		// fetch data
		book := models.NewEmptyBook()
		err = storage.FetchOne(book, models.BookID.String()+" = ?", bookID)
		if err != nil {
			c.JSON(
				http.StatusInternalServerError,
				gin.H{RESPONSE_MESSAGE: err.Error()},
			)
			return
		}

		items := models.NewEmptyItems()
		err = storage.FetchMany(
			&items,
			orderBy,
			first,
			limit,
			isAscending,
			models.ItemBookID.String()+" = ?",
			bookID,
		)

		if err != nil {
			c.JSON(
				http.StatusInternalServerError,
				gin.H{RESPONSE_MESSAGE: err.Error()},
			)
			return
		}

		// drop unnecessary columns
		bookMapTmp := utils.StructToMapInSnake(book)
		bookMap := map[string]interface{}{
			models.BookTitle.String():       bookMapTmp[models.BookTitle.String()],
			models.BookDescription.String(): bookMapTmp[models.BookDescription.String()],
			models.BookCurrencyID.String():  bookMapTmp[models.BookCurrencyID.String()],
		}
		itemMaps := make([]map[string]interface{}, len(items))
		for i, v := range items {
			itemMapTmp := utils.StructToMapInSnake(&v)
			itemMaps[i] = map[string]interface{}{
				models.ItemTitle.String():       itemMapTmp[models.ItemTitle.String()],
				models.ItemDescription.String(): itemMapTmp[models.ItemDescription.String()],
				models.ItemPhotoPath.String():   itemMapTmp[models.ItemPhotoPath.String()],
			}
		}

		// response
		c.JSON(http.StatusOK, gin.H{RESPONSE_BOOK: bookMap, RESPONSE_ITEMS: itemMaps})
	}
}

type jsonBook struct {
	Title       string `json:"title" binding:"required"`
	Description string `json:"description" binding:"required"`
	CurrencyID  int    `json:"currency_id" binding:"required"`
}

func BookPost(storage *models.Storage) gin.HandlerFunc {
	return func(c *gin.Context) {
		// must login
		var err error
		myID, err := middlewares.GetMe(c)
		if err != nil {
			c.JSON(
				http.StatusInternalServerError,
				gin.H{RESPONSE_MESSAGE: err.Error()},
			)
			return
		}

		// parse json
		var json jsonBook
		err = c.ShouldBindJSON(&json)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{RESPONSE_MESSAGE: err.Error()})
			return
		}

		// create
		book := models.NewBook(
			json.Title,
			json.Description,
			json.CurrencyID,
			myID,
		)
		err = storage.Create(book)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{RESPONSE_MESSAGE: err.Error()})
			return
		}

		// response
		c.JSON(http.StatusCreated, gin.H{RESPONSE_MESSAGE: "Successfully created"})
	}
}

func BookPut(storage *models.Storage) gin.HandlerFunc {
	return func(c *gin.Context) {
		// parse query
		var err error
		bookID, err := strconv.Atoi(c.Param(middlewares.PARAM_BOOK))
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{RESPONSE_MESSAGE: err.Error()})
			return
		}

		// parse json
		var json jsonBook
		err = c.ShouldBindJSON(&json)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{RESPONSE_MESSAGE: err.Error()})
			return
		}

		// update
		bookMap := models.NewBookMap(json.Title, json.Description)
		err = storage.UpdateOne(
			models.NewEmptyBook(),
			bookMap,
			models.BookID.String()+" = ?",
			bookID,
		)

		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{RESPONSE_MESSAGE: err.Error()})
			return
		}

		// response
		c.JSON(http.StatusNoContent, gin.H{RESPONSE_MESSAGE: "Successfully updated"})
	}
}

func BookDelete(storage *models.Storage) gin.HandlerFunc {
	return func(c *gin.Context) {
		// parse query
		var err error
		bookID, err := strconv.Atoi(c.Param(middlewares.PARAM_BOOK))
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{RESPONSE_MESSAGE: err.Error()})
			return
		}

		// delete
		tx := storage.WithTransaction()
		book := models.NewEmptyBook()
		err = tx.Delete(book, models.BookID.String()+" = ?", bookID)
		if err != nil {
			tx.Rollback()
			c.JSON(
				http.StatusInternalServerError,
				gin.H{RESPONSE_MESSAGE: err.Error()},
			)
			return
		}

		item := models.NewEmptyItem()
		err = tx.Delete(item, models.BookID.String()+" = ?", bookID)
		if err != nil {
			tx.Rollback()
			c.JSON(
				http.StatusInternalServerError,
				gin.H{RESPONSE_MESSAGE: err.Error()},
			)
			return
		}

		if tx.Commit() != nil {
			tx.Rollback()
			c.JSON(
				http.StatusInternalServerError,
				gin.H{RESPONSE_MESSAGE: err.Error()},
			)
			return
		}

		// response
		c.JSON(http.StatusNoContent, gin.H{RESPONSE_MESSAGE: "Successfully deleted"})
	}
}
