package controllers

import (
	"net/http"

	"github.com/company1101/engel/server/app/middlewares"
	"github.com/company1101/engel/server/app/models"
	"github.com/company1101/engel/server/app/utils"
	"github.com/gin-gonic/gin"
)

func MeGet(storage *models.Storage) gin.HandlerFunc {
	return func(c *gin.Context) {
		var err error
		myID, err := middlewares.GetMe(c)
		if err != nil {
			c.JSON(
				http.StatusInternalServerError,
				gin.H{RESPONSE_MESSAGE: err.Error()},
			)
			return
		}

		// fetch data
		user := models.NewEmptyUser()
		err = storage.FetchOne(user, models.UserID.String()+" = ?", myID)
		if err != nil {
			c.JSON(
				http.StatusInternalServerError,
				gin.H{RESPONSE_MESSAGE: err.Error()},
			)
			return
		}

		// drop unnecessary columns
		userMapTmp := utils.StructToMapInSnake(user)
		userMap := map[string]interface{}{
			models.UserAccountName.String(): userMapTmp[models.UserAccountName.String()], models.UserDisplayName.String(): userMapTmp[models.UserDisplayName.String()],
			models.UserPhotoPath.String(): userMapTmp[models.UserPhotoPath.String()],
		}

		// response
		c.JSON(http.StatusOK, gin.H{RESPONSE_USER: userMap})
	}
}

type jsonMe struct {
	AccountName string `json:"account_name" binding:"required"`
	DisplayName string `json:"display_name" binding:"required"`
	PhotoPath   string `json:"photo_path" binding:"required"`
}

func MePut(storage *models.Storage) gin.HandlerFunc {
	return func(c *gin.Context) {
		var err error
		myID, err := middlewares.GetMe(c)
		if err != nil {
			c.JSON(
				http.StatusInternalServerError,
				gin.H{RESPONSE_MESSAGE: err.Error()},
			)
			return
		}

		// parse json
		var json jsonMe
		err = c.ShouldBindJSON(&json)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{RESPONSE_MESSAGE: err.Error()})
			return
		}

		// update
		user := models.NewUserMap(json.AccountName, json.DisplayName, json.PhotoPath)
		err = storage.UpdateOne(models.NewEmptyUser(), user, models.UserID.String()+" = ?", myID)
		if err != nil {
			c.JSON(
				http.StatusInternalServerError,
				gin.H{RESPONSE_MESSAGE: err.Error()},
			)
			return
		}

		// response
		c.JSON(http.StatusNoContent, gin.H{RESPONSE_MESSAGE: "Successfully updated"})
	}
}

func MeDelete(storage *models.Storage) gin.HandlerFunc {
	return func(c *gin.Context) {
		var err error
		myID, err := middlewares.GetMe(c)
		if err != nil {
			c.JSON(
				http.StatusInternalServerError,
				gin.H{RESPONSE_MESSAGE: err.Error()},
			)
			return
		}

		err = storage.Delete(models.NewEmptyUser(), models.UserID.String()+" = ?", myID)
		if err != nil {
			c.JSON(
				http.StatusInternalServerError,
				gin.H{RESPONSE_MESSAGE: err.Error()},
			)
			return
		}

		// response
		c.JSON(http.StatusNoContent, gin.H{RESPONSE_MESSAGE: "Successfully deleted"})
	}
}
