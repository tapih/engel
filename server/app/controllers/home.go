package controllers

import (
	"net/http"

	"github.com/company1101/engel/server/app/middlewares"
	"github.com/company1101/engel/server/app/models"
	"github.com/company1101/engel/server/app/utils"

	"github.com/gin-gonic/gin"
)

func HomeGet(storage *models.Storage, limit int) gin.HandlerFunc {
	return func(c *gin.Context) {
		var err error

		myID, err := middlewares.GetMe(c)
		if err != nil {
			c.JSON(
				http.StatusInternalServerError,
				gin.H{RESPONSE_MESSAGE: err.Error()},
			)
			return
		}
		orderBy, first, isAscending, err := parsePaginationParams(c)
		if err != nil {
			c.JSON(
				http.StatusBadRequest,
				gin.H{RESPONSE_MESSAGE: err.Error()},
			)
			return
		}

		// if not login
		if myID == 0 {
			c.JSON(http.StatusOK, gin.H{RESPONSE_BOOKS: nil})
		} else {
			members := models.NewEmptyMembers()
			err = storage.FetchMany(
				&members,
				models.MemberCreatedAt.String(),
				nil,
				-1,
				false,
				models.MemberUserID.String()+" = ?",
				myID,
			)
			if err != nil {
				c.JSON(
					http.StatusInternalServerError,
					gin.H{RESPONSE_MESSAGE: err.Error()},
				)
				return
			}
			bookIDs := make([]int, len(members))
			for i, v := range members {
				bookIDs[i] = v.BookID
			}

			books := models.NewEmptyBooks()
			// TODO: why .String() is needed ????
			err = storage.FetchMany(
				&books,
				orderBy,
				first,
				limit,
				isAscending,
				models.BookID.String()+" IN (?)",
				bookIDs,
			)

			if err != nil {
				c.JSON(
					http.StatusInternalServerError,
					gin.H{RESPONSE_MESSAGE: err.Error()},
				)
				return
			}

			// drop unnecessary columns
			bookMaps := make([]map[string]interface{}, len(books))
			for i, v := range books {
				bookMapTmp := utils.StructToMapInSnake(&v)
				bookMaps[i] = map[string]interface{}{
					models.BookTitle.String():       bookMapTmp[models.BookTitle.String()],
					models.BookDescription.String(): bookMapTmp[models.BookDescription.String()],
					models.BookCurrencyID.String():  bookMapTmp[models.BookCurrencyID.String()],
				}
			}

			c.JSON(http.StatusOK, gin.H{RESPONSE_BOOKS: bookMaps})
		}
	}
}
