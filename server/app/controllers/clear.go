package controllers

import (
	"github.com/company1101/engel/server/app/models"
	"github.com/gin-gonic/gin"
)

func ClearPost(storage *models.Storage) gin.HandlerFunc {
	return func(c *gin.Context) {

	}
}

func ClearPut(storage *models.Storage) gin.HandlerFunc {
	return func(c *gin.Context) {

	}
}

func ClearDelete(storage *models.Storage) gin.HandlerFunc {
	return func(c *gin.Context) {

	}
}
