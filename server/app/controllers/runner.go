package controllers

import (
	"fmt"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

type Runner struct {
	Port         int
	ReadTimeout  int
	WriteTimeout int
	WithSSL      bool
	SSLCert      string
	SSLKey       string
}

func NewRunner(
	port int,
	readTimeout, writeTimeout int,
	withSSL bool,
	sslCert, sslKey string,
) *Runner {
	return &Runner{
		Port:         port,
		ReadTimeout:  readTimeout,
		WriteTimeout: writeTimeout,
		WithSSL:      withSSL,
		SSLCert:      sslCert,
		SSLKey:       sslKey,
	}
}

func (c *Runner) Listen(router *gin.Engine) {
	server := &http.Server{
		Handler:        router,
		Addr:           fmt.Sprintf(":%d", c.Port),
		ReadTimeout:    time.Duration(c.ReadTimeout) * time.Second,
		WriteTimeout:   time.Duration(c.WriteTimeout) * time.Second,
		MaxHeaderBytes: 1 << 20,
	}
	if c.WithSSL {
		server.ListenAndServeTLS(c.SSLCert, c.SSLKey)
	} else {
		server.ListenAndServe()
	}
}
