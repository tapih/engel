package controllers

import (
	"net/http"
	"strconv"

	"github.com/company1101/engel/server/app/middlewares"
	"github.com/company1101/engel/server/app/models"
	"github.com/company1101/engel/server/app/utils"
	"github.com/gin-gonic/gin"
)

func ItemGet(storage *models.Storage, limit int) gin.HandlerFunc {
	return func(c *gin.Context) {
		var err error
		itemID, err := strconv.Atoi(c.Param(middlewares.PARAM_ITEM))
		if err != nil {
			c.JSON(
				http.StatusInternalServerError,
				gin.H{RESPONSE_MESSAGE: err.Error()},
			)
			return
		}

		orderBy, first, isAscending, err := parsePaginationParams(c)
		if err != nil {
			c.JSON(
				http.StatusBadRequest,
				gin.H{RESPONSE_MESSAGE: err.Error()},
			)
			return
		}

		item := models.NewEmptyItem()
		err = storage.FetchOne(item, models.ItemID.String()+" = ?", itemID)
		if err != nil {
			c.JSON(
				http.StatusInternalServerError,
				gin.H{RESPONSE_MESSAGE: err.Error()},
			)
			return
		}

		payments := models.NewEmptyPayments()
		err = storage.FetchMany(
			&payments,
			orderBy,
			first,
			limit,
			isAscending,
			models.PaymentID.String()+" = ?",
			item.ID,
		)

		itemMapTmp := utils.StructToMapInSnake(item)
		itemMap := map[string]interface{}{
			models.ItemBookID.String():      itemMapTmp[models.ItemBookID.String()],
			models.ItemTitle.String():       itemMapTmp[models.ItemTitle.String()],
			models.ItemDescription.String(): itemMapTmp[models.ItemDescription.String()],
			models.ItemPhotoPath.String():   itemMapTmp[models.ItemPhotoPath.String()],
		}
		c.JSON(http.StatusOK, gin.H{RESPONSE_FRIEND: itemMap})
	}
}

type jsonItem struct {
	BookID      int    `json:"book_id" binding:"required"`
	Title       string `json:"title" binding:"required"`
	Description string `json:"description" binding:"required"`
	PhotoPath   string `json:"photo_path" binding:"required"`
}

func ItemPost(storage *models.Storage) gin.HandlerFunc {
	return func(c *gin.Context) {
		var err error
		myID, err := middlewares.GetMe(c)
		if err != nil {
			c.JSON(
				http.StatusInternalServerError,
				gin.H{RESPONSE_MESSAGE: err.Error()},
			)
			return
		}

		// parse json
		var json jsonItem
		err = c.ShouldBindJSON(&json)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{RESPONSE_MESSAGE: err.Error()})
			return
		}

		item := models.NewItem(
			json.BookID,
			json.Title,
			json.Description,
			json.PhotoPath,
			myID,
		)
		err = storage.Create(item)
		if err != nil {
			c.JSON(
				http.StatusInternalServerError,
				gin.H{RESPONSE_MESSAGE: err.Error()},
			)
			return
		}

		c.JSON(http.StatusCreated, gin.H{RESPONSE_MESSAGE: "Successfully created"})
	}
}

func ItemPut(storage *models.Storage) gin.HandlerFunc {
	return func(c *gin.Context) {
		var err error
		itemID, err := strconv.Atoi(c.Param(middlewares.PARAM_ITEM))
		if err != nil {
			c.JSON(
				http.StatusInternalServerError,
				gin.H{RESPONSE_MESSAGE: err.Error()},
			)
			return
		}

		// parse json
		var json jsonItem
		err = c.ShouldBindJSON(&json)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{RESPONSE_MESSAGE: err.Error()})
			return
		}

		item := models.NewEmptyItem()
		itemMap := models.NewItemMap(
			json.Title,
			json.Description,
			json.PhotoPath,
		)
		err = storage.UpdateOne(item, itemMap, models.ItemID.String()+" = ?", itemID)
		if err != nil {
			c.JSON(
				http.StatusInternalServerError,
				gin.H{RESPONSE_MESSAGE: err.Error()},
			)
			return
		}

		c.JSON(http.StatusNoContent, gin.H{RESPONSE_MESSAGE: "Successfully updated"})
	}
}

func ItemDelete(storage *models.Storage) gin.HandlerFunc {
	return func(c *gin.Context) {
		var err error
		itemID, err := strconv.Atoi(c.Param(middlewares.PARAM_ITEM))
		if err != nil {
			c.JSON(
				http.StatusBadRequest,
				gin.H{RESPONSE_MESSAGE: err.Error()},
			)
			return
		}

		// parse json
		item := models.NewEmptyItem()
		err = storage.Delete(item, models.ItemID.String()+" = ?", itemID)
		if err != nil {
			c.JSON(
				http.StatusInternalServerError,
				gin.H{RESPONSE_MESSAGE: err.Error()},
			)
			return
		}

		c.JSON(http.StatusNoContent, gin.H{RESPONSE_MESSAGE: "Successfully deleted"})
	}
}
