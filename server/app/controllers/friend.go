package controllers

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/company1101/engel/server/app/middlewares"
	"github.com/company1101/engel/server/app/models"
	"github.com/company1101/engel/server/app/utils"
	"github.com/gin-gonic/gin"
)

func FriendGetMany(storage *models.Storage, limit int) gin.HandlerFunc {
	return func(c *gin.Context) {
		var err error

		myID, err := middlewares.GetMe(c)
		if err != nil {
			c.JSON(
				http.StatusInternalServerError,
				gin.H{RESPONSE_MESSAGE: err.Error()},
			)
			return
		}

		orderBy, first, isAscending, err := parsePaginationParams(c)
		if err != nil {
			c.JSON(
				http.StatusBadRequest,
				gin.H{RESPONSE_MESSAGE: err.Error()},
			)
			return
		}

		// fetch data
		friends := models.NewEmptyFriends()
		err = storage.FetchMany(
			friends,
			models.FriendCreatedAt.String(),
			first,
			-1,
			isAscending,
			fmt.Sprintf(
				"%s = ? OR %s = ?",
				models.FriendRequestedBy.String(),
				models.FriendRequestedBy.String(),
			),
			myID,
			myID,
		)
		if err != nil {
			c.JSON(
				http.StatusInternalServerError,
				gin.H{RESPONSE_MESSAGE: err.Error()},
			)
			return
		}

		friendIDs := make([]int, len(friends))
		for i, v := range friends {
			switch myID {
			case v.RequestedBy:
				friendIDs[i] = v.RequestedTo
			case v.RequestedTo:
				friendIDs[i] = v.RequestedBy
			default:
				c.JSON(
					http.StatusInternalServerError,
					gin.H{RESPONSE_MESSAGE: fmt.Errorf("")},
				)
				return
			}
		}

		users := models.NewEmptyUsers()
		err = storage.FetchMany(
			&users,
			orderBy,
			first,
			limit,
			isAscending,
			models.UserID.String()+" IN (?)",
			friendIDs,
		)

		// drop unnecessary columns
		userMaps := make([]map[string]interface{}, len(users))
		for i, v := range users {
			userMapTmp := utils.StructToMapInSnake(&v)
			userMaps[i] = map[string]interface{}{
				models.UserAccountName.String(): userMapTmp[models.UserAccountName.String()], models.UserDisplayName.String(): userMapTmp[models.UserDisplayName.String()],
				models.UserPhotoPath.String(): userMapTmp[models.UserPhotoPath.String()],
			}
		}

		// response
		c.JSON(http.StatusOK, gin.H{RESPONSE_USERS: userMaps})
	}
}

func FriendRequest(storage *models.Storage) gin.HandlerFunc {
	return func(c *gin.Context) {
		var err error

		friendID, err := strconv.Atoi(c.Param(middlewares.PARAM_FRIEND))
		if err != nil {
			c.JSON(
				http.StatusBadRequest,
				gin.H{RESPONSE_MESSAGE: err.Error()},
			)
			return
		}

		myID, err := middlewares.GetMe(c)
		if err != nil {
			c.JSON(
				http.StatusInternalServerError,
				gin.H{RESPONSE_MESSAGE: err.Error()},
			)
			return
		}

		friend := models.NewFriend(myID, friendID)
		err = storage.Create(friend)
		if err != nil {
			c.JSON(
				http.StatusInternalServerError,
				gin.H{RESPONSE_MESSAGE: err.Error()},
			)
			return
		}

		// response
		c.JSON(http.StatusCreated, gin.H{RESPONSE_MESSAGE: "Successfully requested"})
	}
}

func FriendAccept(storage *models.Storage) gin.HandlerFunc {
	return func(c *gin.Context) {
		var err error

		friendID, err := strconv.Atoi(c.Param(middlewares.PARAM_FRIEND))
		if err != nil {
			c.JSON(
				http.StatusBadRequest,
				gin.H{RESPONSE_MESSAGE: err.Error()},
			)
			return
		}

		myID, err := middlewares.GetMe(c)
		if err != nil {
			c.JSON(
				http.StatusInternalServerError,
				gin.H{RESPONSE_MESSAGE: err.Error()},
			)
			return
		}

		friendMap := models.NewFriendMap()
		err = storage.UpdateOne(
			models.NewEmptyFriend(),
			friendMap,
			fmt.Sprintf(
				"%s = ? AND %s = ?",
				models.FriendRequestedBy.String(),
				models.FriendRequestedTo.String(),
			),
			friendID,
			myID,
		)
		if err != nil {
			c.JSON(
				http.StatusInternalServerError,
				gin.H{RESPONSE_MESSAGE: err.Error()},
			)
			return
		}

		// response
		c.JSON(http.StatusCreated, gin.H{RESPONSE_MESSAGE: "Successfully accepted"})
	}
}

func FriendDelete(storage *models.Storage) gin.HandlerFunc {
	return func(c *gin.Context) {
		var err error

		friendID, err := strconv.Atoi(c.Param(middlewares.PARAM_FRIEND))
		if err != nil {
			c.JSON(
				http.StatusBadRequest,
				gin.H{RESPONSE_MESSAGE: err.Error()},
			)
			return
		}

		myID, err := middlewares.GetMe(c)
		if err != nil {
			c.JSON(
				http.StatusInternalServerError,
				gin.H{RESPONSE_MESSAGE: err.Error()},
			)
			return
		}

		friend := models.NewEmptyFriend()
		err = storage.Delete(
			friend,
			fmt.Sprintf(
				"(%s = ? AND %s = ?) OR (%s = ? AND %s = ?)",
				models.FriendRequestedBy.String(),
				models.FriendRequestedTo.String(),
				models.FriendRequestedBy.String(),
				models.FriendRequestedTo.String(),
			),
			friendID,
			myID,
			myID,
			friendID,
		)
		if err != nil {
			c.JSON(
				http.StatusInternalServerError,
				gin.H{RESPONSE_MESSAGE: err.Error()},
			)
			return
		}

		// response
		c.JSON(http.StatusCreated, gin.H{RESPONSE_MESSAGE: "Successfully deleted"})
	}
}

func FriendSearch(storage *models.Storage) gin.HandlerFunc {
	return func(c *gin.Context) {
		var err error

		term := c.Query(QUERY_FRIEND_SEARCH)

		user := models.NewEmptyUser()
		err = storage.FetchOne(
			&user,
			models.UserAccountName.String()+" = ?",
			term,
		)
		if err != nil {
			c.JSON(
				http.StatusInternalServerError,
				gin.H{RESPONSE_MESSAGE: err.Error()},
			)
			return
		}
		if user == nil {
			c.JSON(
				http.StatusNoContent,
				gin.H{RESPONSE_FRIEND: nil},
			)
			return
		}

		userMapTmp := utils.StructToMapInSnake(user)
		userMap := map[string]interface{}{
			models.UserAccountName.String(): userMapTmp[models.UserAccountName.String()], models.UserDisplayName.String(): userMapTmp[models.UserDisplayName.String()],
			models.UserPhotoPath.String(): userMapTmp[models.UserPhotoPath.String()],
		}
		c.JSON(
			http.StatusOK,
			gin.H{RESPONSE_FRIEND: userMap},
		)
	}
}
