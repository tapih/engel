package controllers

import (
	"net/http"

	"github.com/company1101/engel/server/app/middlewares"
	"github.com/company1101/engel/server/app/services"
	"github.com/gin-contrib/sessions"

	"github.com/company1101/engel/server/app/models"
	"github.com/gin-gonic/gin"
)

// TODO: which status code is best ??
func AuthLineGet(oauth *services.OAuth) gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Redirect(http.StatusFound, oauth.MakeAuthURL())
	}
}

func AuthLineGetCallback(storage *models.Storage, oauth *services.OAuth) gin.HandlerFunc {
	return func(c *gin.Context) {
		var err error
		name := models.OAuthProviderName(oauth.Name)
		err = name.Validate()
		if err != nil {
			c.JSON(
				http.StatusInternalServerError,
				gin.H{RESPONSE_MESSAGE: err.Error()},
			)
			return
		}

		profile, err := oauth.FetchProfile(c.Query("code"))
		if err != nil {
			c.JSON(
				http.StatusInternalServerError,
				gin.H{RESPONSE_MESSAGE: err.Error()},
			)
			return
		}

		// check if the id exists
		accountID := profile["id"]
		account, ok := models.NewEmptyOAuthAccount(name).(*models.LineAccount)
		if !ok {
			c.JSON(
				http.StatusInternalServerError,
				gin.H{RESPONSE_MESSAGE: "Assertion failed"},
			)
			return
		}
		err = storage.FetchOne(account, models.OAuthToken.String()+" = ?", accountID)
		if err != nil {
			c.JSON(
				http.StatusInternalServerError,
				gin.H{RESPONSE_MESSAGE: err.Error()},
			)
			return
		}

		if account != nil {
			if account.UserID != 0 {
				// line account found and user_id is not blank
				sessionPtr := models.NewEmptySession()
				err = storage.Delete(
					sessionPtr,
					models.SessionUserID.String()+" = ?",
					account.UserID,
				)
				if err != nil {
					c.JSON(
						http.StatusInternalServerError,
						gin.H{RESPONSE_MESSAGE: err.Error()},
					)
					return
				}

				newSessionPtr := models.NewSession(account.UserID)
				err := storage.Create(newSessionPtr)
				if err != nil {
					c.JSON(
						http.StatusInternalServerError,
						gin.H{RESPONSE_MESSAGE: err.Error()},
					)
					return
				}

				session := sessions.Default(c)
				session.Set(middlewares.COOKIE_SESSION_ID, newSessionPtr.ID)
				c.JSON(
					http.StatusOK,
					gin.H{RESPONSE_IS_PROFILE_FILLED: true},
				)
			} else {
				// line account found but user_id is blank
				c.JSON(
					http.StatusOK,
					gin.H{RESPONSE_IS_PROFILE_FILLED: false},
				)
			}
		} else {
			// line account is not found
			newAccount, ok := models.NewOAuthAccount(name, accountID).(*models.LineAccount)
			if !ok {
				c.JSON(
					http.StatusInternalServerError,
					gin.H{RESPONSE_MESSAGE: "Assertion failed"},
				)
				return
			}
			err = storage.Create(newAccount)
			if err != nil {
				c.JSON(
					http.StatusInternalServerError,
					gin.H{RESPONSE_MESSAGE: err.Error()},
				)
				return
			}
			c.JSON(
				http.StatusOK,
				gin.H{RESPONSE_IS_PROFILE_FILLED: false},
			)
		}
	}
}

func GetLogout() gin.HandlerFunc {
	return func(c *gin.Context) {
		sess := sessions.Default(c)
		sess.Delete(middlewares.COOKIE_SESSION_ID)
		c.JSON(http.StatusOK, gin.H{RESPONSE_MESSAGE: "Logout succeeded"})
	}
}
