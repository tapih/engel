package controllers

import (
	"fmt"

	"github.com/company1101/engel/server/app/models"
	"github.com/gin-gonic/gin"
)

const QUERY_FRIEND_SEARCH = "term"

const RESPONSE_MESSAGE = "message"

const (
	RESPONSE_BOOK              = "book"
	RESPONSE_BOOKS             = "books"
	RESPONSE_ITEMS             = "items"
	RESPONSE_USER              = "user"
	RESPONSE_USERS             = "users"
	RESPONSE_FRIEND            = "friend"
	RESPONSE_IS_PROFILE_FILLED = "is_profile_filled"
)

func parsePaginationParams(c *gin.Context) (string, interface{}, bool, error) {
	var isAscending bool
	orderBy := c.DefaultQuery("orderBy", models.GORM_CREATED_AT)
	firstTmp := c.DefaultQuery("first", "")

	var first interface{}
	first = firstTmp
	if firstTmp == "" {
		first = nil
	}

	isAscendingTmp := c.DefaultQuery("order", "desc")
	switch isAscendingTmp {
	case "asc":
		isAscending = true
	case "desc":
		isAscending = false
	default:
		return "", nil, false, fmt.Errorf("order should be asc or desc")
	}
	return orderBy, first, isAscending, nil
}
