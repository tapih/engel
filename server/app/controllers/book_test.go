package controllers_test

import (
	"context"
	"fmt"
	"io/ioutil"
	"net/http"
	"testing"

	"github.com/company1101/engel/server/app/controllers"
	"github.com/company1101/engel/server/app/middlewares"

	"github.com/company1101/engel/server/app/models"
	"github.com/company1101/engel/server/app/utils"
	"github.com/gin-gonic/gin"
)

var books = [][]interface{}{
	[]interface{}{"lunch11", "1/1", 1, 1},
	[]interface{}{"lunch12", "1/2", 2, 1},
	[]interface{}{"lunch13", "1/3", 3, 1},
	[]interface{}{"lunch14", "1/4", 4, 2},
	[]interface{}{"lunch15", "1/5", 5, 2},
	[]interface{}{"lunch16", "1/6", 1, 2},
	[]interface{}{"lunch17", "1/7", 2, 3},
	[]interface{}{"lunch18", "1/8", 3, 3},
	[]interface{}{"lunch19", "1/9", 4, 4},
	[]interface{}{"lunch21", "2/1", 5, 4},
}

var items = [][]interface{}{
	[]interface{}{1, "lunch11", "1/1", "img1.jpg", 1},
	[]interface{}{1, "lunch12", "1/2", "img2.jpg", 2},
	[]interface{}{2, "lunch13", "1/3", "img3.jpg", 3},
	[]interface{}{2, "lunch14", "1/4", "img4.jpg", 4},
	[]interface{}{3, "lunch15", "1/5", "img5.jpg", 5},
	[]interface{}{3, "lunch16", "1/6", "img6.jpg", 1},
	[]interface{}{3, "lunch17", "1/7", "img7.jpg", 2},
	[]interface{}{4, "lunch18", "1/8", "img8.jpg", 3},
	[]interface{}{5, "lunch19", "1/9", "img9.jpg", 4},
	[]interface{}{6, "lunch21", "2/1", "img0.jpg", 5},
}

func initBookTest(storage *models.Storage, t *testing.T) ([]models.Book, []models.Item) {
	var err error
	bookPtr := models.NewEmptyBook()
	err = models.RefreshDatabase(storage, bookPtr)
	if err != nil {
		utils.TestFailedWithError(t, err)
	}

	booksCreated, err := models.CreateBooks(storage, bookPtr, books)
	if err != nil {
		utils.TestFailedWithError(t, err)
	}

	itemPtr := models.NewEmptyItem()
	err = models.RefreshDatabase(storage, itemPtr)
	if err != nil {
		utils.TestFailedWithError(t, err)
	}

	itemsCreated, err := models.CreateItems(storage, itemPtr, items)
	if err != nil {
		utils.TestFailedWithError(t, err)
	}
	return booksCreated, itemsCreated
}

var limitBooks = 10

func getBook(t *testing.T, id int) string {
	t.Helper()
	resp, err := http.Get(fmt.Sprintf("https://localhost:8280/book/%d", id))
	if err != nil {
		utils.TestFailedWithError(t, err)
	}
	defer resp.Body.Close()
	byteArray, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		utils.TestFailedWithError(t, err)
	}
	return string(byteArray)
}

func TestBookGet(t *testing.T) {
	booksCreated, itemsCreated := initBookTest(storage, t)
	ctx, cancel := context.WithCancel(context.Background())
	go func(c context.Context) {
		router := gin.New()
		router.GET("/book/:"+middlewares.PARAM_BOOK, controllers.BookGet(storage, limitBooks))
		runner.Listen(router)
	}(ctx)

	book1 := getBook(t, 1)
	fmt.Println(book1)
	fmt.Println(booksCreated)
	fmt.Println(itemsCreated)
	// book2 := getBook(t, 2)
	// book8 := getBook(t, 8)
	cancel()
	// select {
	// case <-ctx.Done():
	// 	utils.AssertMapEqual(t, book1, map[string]interface{}{
	// 		controllers.RESPONSE_BOOK: utils.StructToMapInSnake(booksCreated[0]),
	// 		controllers.RESPONSE_ITEMS: []map[string]interface{}{
	// 			utils.StructToMapInSnake(booksCreated[0]),
	// 			utils.StructToMapInSnake(booksCreated[1]),
	// 		},
	// 	})
	// }
}
