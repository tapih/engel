package middlewares

import (
	"fmt"
	"net/http"

	"github.com/company1101/engel/server/app/models"
	"github.com/gin-gonic/gin"
)

func NeedHaveBook(storage *models.Storage) gin.HandlerFunc {
	return func(c *gin.Context) {
		bookID := c.Param(PARAM_BOOK)

		var err error
		// need login
		userID, err := GetMe(c)
		if err != nil {
			c.JSON(
				http.StatusInternalServerError,
				gin.H{RESPONSE_MESSAGE: err.Error()},
			)
			return
		}

		member := models.NewEmptyMember()
		err = storage.FetchOne(
			member,
			fmt.Sprintf(
				"%s = ? AND %s = ?",
				models.MemberBookID.String(),
				models.MemberUserID.String(),
			),
			bookID,
			userID,
		)
		if err != nil {
			c.JSON(
				http.StatusInternalServerError,
				gin.H{RESPONSE_MESSAGE: err.Error()},
			)
			return
		}
		if member == nil {
			c.JSON(
				http.StatusNotFound,
				gin.H{RESPONSE_MESSAGE: "not found"},
			)
			return
		}
		c.Next()
	}
}

func NeedHaveItem(storage *models.Storage) gin.HandlerFunc {
	return func(c *gin.Context) {
		itemID := c.Param(PARAM_ITEM)

		var err error
		// need login
		userID, err := GetMe(c)
		if err != nil {
			c.JSON(
				http.StatusInternalServerError,
				gin.H{RESPONSE_MESSAGE: err.Error()},
			)
			return
		}

		item := models.NewEmptyItem()
		err = storage.FetchOne(item, models.ItemID.String()+" = ?", itemID)
		if err != nil {
			c.JSON(
				http.StatusInternalServerError,
				gin.H{RESPONSE_MESSAGE: err.Error()},
			)
			return
		}
		if item == nil {
			c.JSON(
				http.StatusNotFound,
				gin.H{RESPONSE_MESSAGE: "not found"},
			)
			return
		}

		member := models.NewEmptyMember()
		err = storage.FetchOne(
			member,
			fmt.Sprintf(
				"%s = ? AND %s = ?",
				models.MemberBookID.String(),
				models.MemberUserID.String(),
			),
			item.BookID,
			userID,
		)
		if err != nil {
			c.JSON(
				http.StatusInternalServerError,
				gin.H{RESPONSE_MESSAGE: err.Error()},
			)
			return
		}
		if member == nil {
			c.JSON(
				http.StatusNotFound,
				gin.H{RESPONSE_MESSAGE: "not found"},
			)
			return
		}

		c.Keys[KEYS_ITEM] = item
		c.Next()
	}
}

func NeedIsFriend(storage *models.Storage) gin.HandlerFunc {
	return func(c *gin.Context) {
		friendID := c.Param(PARAM_FRIEND)

		var err error
		// need login
		myID, err := GetMe(c)
		if err != nil {
			c.JSON(
				http.StatusInternalServerError,
				gin.H{RESPONSE_MESSAGE: err.Error()},
			)
			return
		}

		friend := models.NewEmptyFriend()
		err = storage.FetchOne(
			friend,
			fmt.Sprintf(
				"(%s = ? AND %s = ?) OR (%s = ? AND %s = ?)",
				models.FriendRequestedBy.String(),
				models.FriendRequestedTo.String(),
				models.FriendRequestedBy.String(),
				models.FriendRequestedTo.String(),
			),
			myID,
			friendID,
			friendID,
			myID,
		)
		if err != nil {
			c.JSON(
				http.StatusInternalServerError,
				gin.H{RESPONSE_MESSAGE: err.Error()},
			)
			return
		}
		if friend == nil {
			c.JSON(
				http.StatusNotFound,
				gin.H{RESPONSE_MESSAGE: "not found"},
			)
			return
		}

		c.Next()
	}
}
