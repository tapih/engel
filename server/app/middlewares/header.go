package middlewares

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func HeaderHandler(host string) gin.HandlerFunc {
	return func(c *gin.Context) {
		// request
		// csrf
		// NOTE: http://d.hatena.ne.jp/hasegawayosuke/20130302/p1
		hostReq := c.GetHeader("Host")
		originReq := c.GetHeader("origin")
		xFromReq := c.GetHeader("X-From")
		if hostReq != host ||
			xFromReq == "" ||
			(originReq != "" && xFromReq != originReq) {
			c.JSON(http.StatusBadRequest, gin.H{RESPONSE_MESSAGE: "CSRF attack was detected"})
			return
		}

		c.Next()

		// response
		// NOTE: https://qiita.com/sooogle/items/c066b0d69a81370653f7
		c.Header("X-Frame-Options", "SAMEORIGIN")
		c.Header("X-XSS-Protection", "1; mode=block")
		c.Header("X-Content-Type-Options", "nosniff")
		c.Header("Referrer-Policy", "strict-origin-when-cross-origin")
	}
}
