package middlewares

import (
	"fmt"

	"github.com/company1101/engel/server/app/models"
	"github.com/gin-gonic/gin"
)

const RESPONSE_MESSAGE = "message"

const (
	PARAM_BOOK   = "book_id"
	PARAM_ITEM   = "item_id"
	PARAM_FRIEND = "friend_id"
)

const (
	KEYS_BOOK       = "book"
	KEYS_ITEM       = "item"
	KEYS_FRIEND     = "friend"
	KEYS_ME         = "me"
	KEYS_MEMBER     = "member"
	KEYS_SESSION_ID = "session_id"
)

const COOKIE_SESSION_ID = "session_id"

func GetMe(c *gin.Context) (int, error) {
	var ok bool
	meTmp, ok := c.Keys[KEYS_ME]
	if !ok {
		return 0, fmt.Errorf(KEYS_ME + " cannot found in keys")
	}
	me, ok := meTmp.(*models.User)
	if !ok {
		return 0, fmt.Errorf("Failed type assertion")
	}
	return me.ID, nil
}
