package middlewares

import (
	"net/http"

	"github.com/company1101/engel/server/app/models"
	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/sessions/cookie"
	"github.com/gin-gonic/gin"
)

func Session(
	name string,
	path string,
	domain string,
	maxAge int,
	secure bool,
	httpOnly bool,
	secret string,
) gin.HandlerFunc {
	storage := cookie.NewStore([]byte(secret))
	storage.Options(sessions.Options{
		Path:     path,
		Domain:   domain,
		MaxAge:   maxAge,
		Secure:   secure,
		HttpOnly: httpOnly,
	})
	return sessions.Sessions(name, storage)
}

func NeedLogin(storage *models.Storage) gin.HandlerFunc {
	return func(c *gin.Context) {
		var err error
		session := sessions.Default(c)
		v, ok := session.Get(COOKIE_SESSION_ID).(string)
		if !ok {
			c.JSON(http.StatusInternalServerError, gin.H{RESPONSE_MESSAGE: "Assertion failed"})
			return
		}
		if v == "" {
			c.JSON(http.StatusUnauthorized, gin.H{RESPONSE_MESSAGE: err.Error()})
			return
		}

		sessionPtr := models.NewEmptySession()
		err = storage.FetchOne(sessionPtr, models.SessionID.String()+" = ?", v)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{RESPONSE_MESSAGE: err.Error()})
			return
		}
		if sessionPtr == nil {
			c.JSON(http.StatusUnauthorized, gin.H{RESPONSE_MESSAGE: "Not login"})
			return
		}

		user := models.NewEmptyUser()
		err = storage.FetchOne(user, models.UserID.String()+" = ?", sessionPtr.UserID)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{RESPONSE_MESSAGE: err.Error()})
			return
		}
		c.Keys[KEYS_ME] = user
		c.Next()
	}
}

func MightLogin(storage *models.Storage) gin.HandlerFunc {
	return func(c *gin.Context) {
		var err error
		session := sessions.Default(c)
		v, ok := session.Get(COOKIE_SESSION_ID).(string)
		if !ok {
			c.JSON(http.StatusInternalServerError, gin.H{RESPONSE_MESSAGE: "Assertion failed"})
			return
		}

		if v == "" {
			c.Keys[KEYS_ME] = models.NewEmptyUser()
		} else {
			sessionPtr := models.NewEmptySession()
			err = storage.FetchOne(sessionPtr, models.SessionID.String()+" = ?", v)
			if err != nil {
				c.JSON(http.StatusInternalServerError, gin.H{RESPONSE_MESSAGE: err.Error()})
				return
			}

			user := models.NewEmptyUser()
			err = storage.FetchOne(user, models.UserID.String()+" = ?", sessionPtr.UserID)
			if err != nil {
				c.JSON(http.StatusInternalServerError, gin.H{RESPONSE_MESSAGE: err.Error()})
				return
			}
			c.Keys[KEYS_ME] = user
		}
		c.Next()
	}
}
