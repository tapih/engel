package utils

import (
	"fmt"
	"testing"
)

// error handlers
func TestFailed(t *testing.T) {
	t.Helper()
	t.Fatalf("test failed")
}

func TestFailedWithError(t *testing.T, err error) {
	t.Helper()
	t.Fatalf("test failed (Error): %#v", err)
}

func TestFailedNotEqual(t *testing.T, v1, v2 interface{}) {
	t.Helper()
	t.Fatalf("test failed (Not equal): %#v %#v", v1, v2)
}

func TestFailedLengthNotEqual(t *testing.T, v1, v2 int) {
	t.Helper()
	t.Fatalf("test failed (Length mismatch): %d %d", v1, v2)
}

// Assertions
func AssertLengthEqual(t *testing.T, v1, v2 int) {
	t.Helper()
	if v1 != v2 {
		TestFailedLengthNotEqual(t, v1, v2)
	}
}

func AssertValueEqual(t *testing.T, v1, v2 interface{}) {
	t.Helper()
	if v1 != v2 {
		TestFailedNotEqual(t, v1, v2)
	}
}

func AssertModelEqual(t *testing.T, c1, c2 interface{}) {
	t.Helper()
	m1 := StructToMapInSnake(c1)
	m2 := StructToMapInSnake(c2)

	for k, v1 := range m1 {
		if k != "created_at" && k != "deleted_at" && k != "updated_at" {
			if v2, ok := m2[k]; ok {
				if v1 != v2 {
					TestFailedNotEqual(t, c1, c2)
				}
			} else {
				TestFailedWithError(t, fmt.Errorf("Field Mismatch"))
			}
		}
	}
}

func AssertMapEqual(t *testing.T, c1, c2 map[string]interface{}) {
	t.Helper()
	for k, v1 := range c1 {
		if v2, ok := c2[k]; ok {
			if v1 != v2 {
				TestFailedNotEqual(t, c1, c2)
			}
		} else {
			TestFailedWithError(t, fmt.Errorf("Field Mismatch"))
		}
	}
}
