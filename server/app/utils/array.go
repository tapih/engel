package utils

import (
	"fmt"
	"math/rand"
)

func Arrange(to int) []int {
	var ids []int
	for i := 0; i < to; i++ {
		ids = append(ids, i+1)
	}
	return ids
}

func Contain(arr []int, target int) bool {
	isContained := false
	for _, v := range arr {
		if v == target {
			isContained = true
			break
		}
	}
	return isContained
}

func ContainStr(arr []string, target string) bool {
	isContained := false
	for _, v := range arr {
		if v == target {
			isContained = true
			break
		}
	}
	return isContained
}

func SelectSome(max int, num int) ([]int, error) {
	if max < num {
		return nil, fmt.Errorf("num > max")
	}
	// isAboveHalf := max/2 < num
	var result []int
	count := 0
	for v := rand.Intn(max); count < num; {
		if !Contain(result, v) {
			result = append(result, v)
		}
	}
	return result, nil
}
