package utils

type EnumNotContained struct {
	Value string
}

func (e EnumNotContained) Error() string {
	return "Invalid enum value: " + e.Value
}
