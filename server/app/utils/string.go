package utils

import "github.com/segmentio/ksuid"

func MakeUniqString() string {
	return ksuid.New().String()
}
