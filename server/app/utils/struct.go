package utils

import (
	"reflect"

	"github.com/iancoleman/strcase"
)

func StructToMapInSnake(data interface{}) map[string]interface{} {
	result := make(map[string]interface{})
	elem := reflect.ValueOf(data).Elem()
	size := elem.NumField()

	for i := 0; i < size; i++ {
		field := strcase.ToSnake(elem.Type().Field(i).Name)
		value := elem.Field(i).Interface()
		result[field] = value
	}

	return result
}
