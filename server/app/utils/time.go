package utils

import "time"

func ConvertMaxAgeToTime(maxAge int) time.Time {
	return time.Now().Add(-time.Duration(maxAge) * time.Second)
}
