package main

import (
	"log"

	"github.com/company1101/engel/server/app/middlewares"

	"github.com/company1101/engel/server/app/configs"
	"github.com/company1101/engel/server/app/controllers"

	"github.com/company1101/engel/server/app/models"
	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
)

func main() {
	var err error
	args, err := configs.ParseArgs()
	if err != nil {
		panic(err)
	}

	if args.IsProd() {
		gin.SetMode(gin.ReleaseMode)
	}

	path := args.GetConfigPath()
	conf, err := configs.LoadConfig(path)
	if err != nil {
		panic(err)
	}

	storage, err := conf.DB.NewStorage()
	if err != nil {
		panic(err)
	}

	if args.IsDevel() && args.DropTables {
		log.Println("All tables have been dropped.")
		err = models.DropTablesIfExists(storage)
		if err != nil {
			panic(err)
		}
	}

	err = models.CreateTablesIfNotExists(storage)
	if err != nil {
		panic(err)
	}

	// TODO:  limit
	limit := 10
	router := gin.New()
	router.Use(gin.Recovery())
	router.Use(conf.Log.NewMiddleware())
	router.Use(conf.Session.NewMiddleware())

	// add handlers
	apiRoot := router.Group("/api/v1")
	{
		apiHome := apiRoot.Group("/home")
		apiHome.Use(middlewares.MightLogin(storage))
		apiHome.GET("/", controllers.HomeGet(storage, limit))
	}
	{
		var name string
		name = models.OAuthProviderLine.String()
		lineConf := conf.OAuth[name]
		lineOAuth := lineConf.NewHandler(name)
		apiLine := apiRoot.Group("/auth/line")
		apiLine.GET("/", controllers.AuthLineGet(lineOAuth))
		apiLine.GET("/callback", controllers.AuthLineGetCallback(storage, lineOAuth))
	}

	apiLogin := apiRoot.Group("/")
	apiLogin.Use(middlewares.NeedLogin(storage))
	apiLogin.GET("/logout", controllers.GetLogout())
	{
		apiBook := apiLogin.Group("/book")
		apiBook.Use(middlewares.NeedHaveBook(storage))
		apiBook.GET("/:"+middlewares.PARAM_BOOK, controllers.BookGet(storage, limit))
		apiBook.POST("/", controllers.BookPost(storage))
		apiBook.PUT("/:"+middlewares.PARAM_BOOK, controllers.BookPut(storage))
		apiBook.DELETE("/:"+middlewares.PARAM_BOOK, controllers.BookDelete(storage))
	}
	{
		apiItem := apiLogin.Group("/item")
		apiItem.Use(middlewares.NeedHaveItem(storage))
		apiItem.GET("/:"+middlewares.PARAM_ITEM, controllers.ItemGet(storage, limit))
		apiItem.POST("/", controllers.ItemPost(storage))
		apiItem.PUT("/:"+middlewares.PARAM_ITEM, controllers.ItemPut(storage))
		apiItem.DELETE("/:"+middlewares.PARAM_ITEM, controllers.ItemDelete(storage))
	}
	{
		apiMe := apiLogin.Group("/me")
		apiMe.GET("/", controllers.MeGet(storage))
		apiMe.PUT("/", controllers.MePut(storage))
		apiMe.DELETE("/", controllers.MeDelete(storage))
	}
	{
		apiFriend := apiLogin.Group("/friend")
		apiFriend.Use(middlewares.NeedIsFriend(storage))
		apiFriend.GET("/", controllers.FriendGetMany(storage, limit))
		apiFriend.POST("/:"+middlewares.PARAM_FRIEND+"/request", controllers.FriendRequest(storage))
		apiFriend.POST("/:"+middlewares.PARAM_FRIEND+"/accept", controllers.FriendAccept(storage))
		apiFriend.DELETE("/:"+middlewares.PARAM_FRIEND, controllers.FriendDelete(storage))
		apiFriend.GET("/search", controllers.FriendSearch(storage))
	}

	// listen ...
	if args.IsDevel() && conf.Http.WithSSL {
		log.Println("Gin's SSL was disabled, but you can access with SSL through proxy)")
		conf.Http.WithSSL = false
	}
	runner := conf.Http.NewRunner()
	runner.Listen(router)
}
