export const API_ENDPOINT = '/api/v1/'

export enum IconSize {
    Smallest = 1.1,
    Small = 1.3,
    Normal = 1.6,
    Large = 2.0,
    Largest = 2.5,
}

export enum FontSize {
    Smallest = 1.0,
    Small = 1.2,
    Normal = 1.5,
    Large = 1.8,
    Largest = 2.2,
}

export enum FontSizeHeading {
    Smallest = 1.6,
    Small = 1.8,
    Normal = 2.2,
    Large = 2.5,
    Largest = 3.0,
}

export enum ZIndex {
    Highest = 100,
    High = 50,
    Middle = 0,
    Low = -50,
    Lowest = -100,
}

export const PRIMARY_COLOR = '#1890ff'
export const TEXT_COLOR_BLACK = 'rgba(0, 0, 0, 0.65)'
export const TEXT_COLOR_WHITE = 'hsla(0,0%,100%,.65)'
