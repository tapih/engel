import styled from 'styled-components'
import { Link } from 'react-router-dom'

import { MoveUpOnHover } from 'components/shared/mixins'

export interface IProps {
    to: string
    width: number
    height: number
}

const App = styled(Link)`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    width: ${(props: IProps) => props.width}rem;
    height: ${(props: IProps) => props.height}rem;
    border-radius: 3px;
    ${MoveUpOnHover};
`
export default App
