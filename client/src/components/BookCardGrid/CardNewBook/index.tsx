import * as React from 'react'
import styled from 'styled-components'
import { Icon } from 'antd'
import { lighten } from 'polished'

import CardBase, { IProps as IBaseProps } from '../CardBase'

import { ROUTE_BOOK_NEW } from 'constants/routes'
import { PRIMARY_COLOR, IconSize } from 'constants/styles'

export type IProps = Pick<IBaseProps, 'width' | 'height'>

const App = (props: IProps) => (
    <Card to={ROUTE_BOOK_NEW} {...props}>
        <Icon
            type="plus"
            theme="outlined"
            style={{
                color: 'white',
                fontSize: IconSize.Largest.toString() + 'rem',
            }}
        />
        <Message>新しい家計簿を追加</Message>
    </Card>
)

const Card = styled(CardBase)`
    background-color: ${PRIMARY_COLOR};
    transition: all 0.2s;
    &:hover {
        background-color: ${lighten(0.1)(PRIMARY_COLOR)};
    }
`

const Message = styled.p`
    margin-top: 1rem;
    color: white;
`

export default App
