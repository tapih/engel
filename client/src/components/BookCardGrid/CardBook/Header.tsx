import * as React from 'react'
import styled from 'styled-components'

import { Ellipsis } from 'components/shared/mixins'
import { FontSize, FontSizeHeading } from 'constants/styles'

export interface IProps {
    title: string
    description: string
}

const App = ({ title, description }: IProps) => (
    <React.Fragment>
        <Title>{title}</Title>
        <Description>{description}</Description>
    </React.Fragment>
)

const Title = styled.h2`
    width: 100%;
    font-size: ${FontSizeHeading.Normal}rem;
    ${Ellipsis};
`

const Description = styled.p`
    width: 100%;
    font-size: ${FontSize.Normal}rem;
    ${Ellipsis};
`
export default App
