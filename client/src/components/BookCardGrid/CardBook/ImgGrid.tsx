import * as React from 'react'
import styled from 'styled-components'

export interface IProps {
    imgs: string[]
}

const numPerRow = 4
const numRow = 2
const gutter = 0.2
const imgSquare = 5

const App = (props: IProps) => {
    const numTotal = numPerRow * numRow
    const boxWidth = numPerRow * (imgSquare + gutter * 2)
    const boxHeight = numRow * (imgSquare + gutter) + gutter // consider overwrap
    return (
        <List width={boxWidth} height={boxHeight}>
            {props.imgs.slice(0, numTotal).map((value, index) => (
                <Item gutter={gutter} key={index}>
                    {index === numTotal - 1 ? (
                        <DummyImg square={imgSquare}>
                            {'+' +
                                (props.imgs.length - numTotal + 1).toString() +
                                '名'}
                        </DummyImg>
                    ) : (
                        <RoundImg
                            square={imgSquare}
                            src={value}
                            alt={'Photo ' + index.toString()}
                        />
                    )}
                </Item>
            ))}
        </List>
    )
}

interface IListProps {
    width: number
    height: number
}

const List = styled.ul`
    width: ${(props: IListProps) => props.width}rem;
    height: ${(props: IListProps) => props.height}rem;
    padding: 0;
`

interface IItemProps {
    gutter: number
}

const Item = styled.li`
    display: inline-block;
    list-style: none;
    margin: ${(props: IItemProps) => props.gutter}rem;
`

interface IDummyImg {
    square: number
}

const DummyImg = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    width: ${(props: IDummyImg) => props.square}rem;
    height: ${(props: IDummyImg) => props.square}rem;
    border-radius: 100%;
`

interface IRoundImg {
    square: number
}

const RoundImg = styled.img`
    width: ${(props: IRoundImg) => props.square}rem;
    height: ${(props: IRoundImg) => props.square}rem;
    && {
        border-radius: 100%;
    }
`

export default App
