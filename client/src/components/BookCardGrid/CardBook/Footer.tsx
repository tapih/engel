import * as React from 'react'
import styled from 'styled-components'
import { Button, Statistic } from 'antd'

export interface IProps {
    amountDidPay: number
    amountWillAccept: number
    linkToNewItem: string
}

const App = (props: IProps) => (
    <Container>
        <Statistic value={props.amountDidPay} suffix="円" />
        <Statistic value={props.amountWillAccept} suffix="円" />
        <Statistic
            value={props.amountWillAccept - props.amountDidPay}
            suffix="円"
        />
        <Button
            type="primary"
            shape="circle"
            icon="plus"
            href={props.linkToNewItem}
        />
    </Container>
)

const Container = styled.div``

export default App
