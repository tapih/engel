import * as React from 'react'
import styled from 'styled-components'

import CardBase, { IProps as IBaseProps } from '../CardBase'
import Header, { IProps as IHeaderProps } from './Header'
import ImgGrid, { IProps as IGridProps } from './ImgGrid'
import Footer, { IProps as IFooterProps } from './Footer'

import { TEXT_COLOR_BLACK } from 'constants/styles'

type IProps = IBaseProps & IHeaderProps & IGridProps & IFooterProps

const App = ({
    to,
    width,
    height,
    title,
    description,
    imgs,
    amountDidPay,
    amountWillAccept,
    linkToNewItem,
}: IProps) => (
    <Card to={to} width={width} height={height}>
        <HeaderContainer>
            <Header title={title} description={description} />
        </HeaderContainer>
        <ContentsContainer>
            <ImgGrid imgs={imgs} />
        </ContentsContainer>
        <FooterContainer>
            <Footer
                amountDidPay={amountDidPay}
                amountWillAccept={amountWillAccept}
                linkToNewItem={linkToNewItem}
            />
        </FooterContainer>
    </Card>
)

const Card = styled(CardBase)`
    padding: 2rem 0;
    background-color: white;
    color: ${TEXT_COLOR_BLACK};
    transition: all 0.2s;
    &:hover,
    &:active {
        color: ${TEXT_COLOR_BLACK};
        opacity: 0.9;
    }
`

const HeaderContainer = styled.header`
    width: 80%;
    align-self: flex-start;
    margin: 0 auto;
    margin-bottom: 1.6rem;
`

const ContentsContainer = styled.div`
    margin-bottom: 1.6rem;
`

const FooterContainer = styled.div``

export default App
