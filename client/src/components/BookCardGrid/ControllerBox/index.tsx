import * as React from 'react'
import styled from 'styled-components'
import { Select, Input } from 'antd'

const { Option } = Select
const { Search } = Input

const App = () => (
    <Container>
        <Search placeholder="検索" style={{ width: 250 }} />
        <Select defaultValue="updated_at_new" style={{ width: 180 }}>
            <Option value="updated_at_new">更新日時が新しい順</Option>
            <Option value="updated_at_old">更新日時が古い順</Option>
            <Option value="created_at_new">作成日時が新しい順</Option>
            <Option value="created_at_old">作成日時が古い順</Option>
            <Option value="total_large">差引額が多い順</Option>
            <Option value="total_small">差引額が少ない順</Option>
        </Select>
    </Container>
)

const Container = styled.div`
    display: flex;
    justify-content: flex-end;
    margin-top: 1rem;
`

export default App
