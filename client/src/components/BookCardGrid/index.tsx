import * as React from 'react'
import styled from 'styled-components'

import ControllerBox from './ControllerBox'
import CardBook from './CardBook'
import CardNewBook from './CardNewBook'

interface IProps {
    numPerRow: number
    innerWidth: number
    innerHeight: number
    gutter: number
}

const Book = (
    <CardBook
        to="/book/1"
        width={32}
        height={43}
        title="tatwafasfas"
        description="sdffasfadfafa"
        imgs={[
            'https://randomuser.me/api/portraits/med/men/65.jpg',
            'https://randomuser.me/api/portraits/med/men/66.jpg',
            'https://randomuser.me/api/portraits/med/men/67.jpg',
            'https://randomuser.me/api/portraits/med/men/68.jpg',
            'https://randomuser.me/api/portraits/med/men/69.jpg',
            'https://randomuser.me/api/portraits/med/men/70.jpg',
            'https://randomuser.me/api/portraits/med/men/71.jpg',
            'https://randomuser.me/api/portraits/med/men/72.jpg',
            'https://randomuser.me/api/portraits/med/men/73.jpg',
        ]}
        amountDidPay={100000}
        amountWillAccept={100000}
        linkToNewItem="/book/1/item/new"
    />
)

const App = (props: IProps) => {
    const outerWidth = (props.innerWidth + props.gutter * 2) * props.numPerRow
    return (
        <React.Fragment>
            <ControllerBox />
            <List width={outerWidth}>
                <Item key={0} gutter={props.gutter}>
                    <CardNewBook
                        width={props.innerWidth}
                        height={props.innerHeight}
                    />
                </Item>
                <Item key={1} gutter={props.gutter}>
                    {Book}
                </Item>
                <Item key={2} gutter={props.gutter}>
                    {Book}
                </Item>
            </List>
        </React.Fragment>
    )
}

// NOTE:
// flex-wrap: wrap+jusitfy-content: center centerize not only list but also item
interface IList {
    width: number
}

const List = styled.ul`
    display: flex;
    align-items: flex-start;
    flex-wrap: wrap;
    width: ${(props: IList) => props.width}rem;
    margin: 0 auto;
    padding: 0;
`

interface IItem {
    gutter: number
}

const Item = styled.li`
    margin: ${(props: IItem) => props.gutter}rem;
`

export default App
