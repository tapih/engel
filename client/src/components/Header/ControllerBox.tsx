import * as React from 'react'
import { Icon, Dropdown, Menu } from 'antd'
import styled from 'styled-components'

import { FontSize } from 'constants/styles'

const App = () => (
    <Container>
        <Button>
            <Icon
                type="mail"
                theme="filled"
                style={{
                    fontSize: '2rem',
                    color: 'white',
                }}
            />
        </Button>
        <Dropdown
            placement="bottomCenter"
            overlay={
                <Menu>
                    <Menu.Item key={1}>
                        <Icon type="setting" />
                        Setting
                    </Menu.Item>
                    <Menu.Item key={2}>
                        <Icon type="logout" />
                        Logout
                    </Menu.Item>
                </Menu>
            }
        >
            <UserBox>
                <SelfPhoto
                    src="https://randomuser.me/api/portraits/med/men/65.jpg"
                    alt="Profile photo"
                />
                <UserName>Tapi tapipero</UserName>
            </UserBox>
        </Dropdown>
    </Container>
)

const Container = styled.div`
    display: flex;
`

const Button = styled.button`
    margin-right: 3rem;
    cursor: pointer;
`

const UserBox = styled.div`
	margin-left; 3rem;
`

const SelfPhoto = styled.img`
    width: 4rem;
    height: 4rem;
    border-radius: 100%;
`

const UserName = styled.span`
    margin-left: 1rem;
    color: white;
    font-size: ${FontSize.Normal}rem;
    cursor: default;
`

export default App
