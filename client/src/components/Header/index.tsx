import * as React from 'react'
import { Layout } from 'antd'

import LogoBox from './LogoBox'
import ControllerBox from './ControllerBox'
import PaymentBox from './PaymentBox'

const App = () => (
    <Layout.Header
        style={{
            width: '100%',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'space-between',
        }}
    >
        <LogoBox />
        <PaymentBox />
        <ControllerBox />
    </Layout.Header>
)

export default App
