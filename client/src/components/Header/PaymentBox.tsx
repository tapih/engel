import * as React from 'react'
import { Statistic } from 'antd'
import styled from 'styled-components'

import { FontSize, TEXT_COLOR_WHITE } from 'constants/styles'

const App = () => (
    <Container>
        <StyledStatistics value={10000} title="支払" />
        <StyledStatistics value={10000} title="受取" />
        <StyledStatistics value={10000} title="合計" color="white" />
    </Container>
)

const Container = styled.div`
    display: flex;
    align-items: center;
    margin-left: auto;
    margin-right: 3rem;
`

interface IStatistics {
    value: number
    title: string
    color?: string
}

const StyledStatistics = (props: IStatistics) => (
    <Statistic
        value={props.value}
        title={props.title}
        style={{
            marginRight: '1rem',
            lineHeight: 1,
            color: props.color || TEXT_COLOR_WHITE,
            width: '7rem',
            fontSize: FontSize.Small.toString() + 'rem',
        }}
        valueStyle={{
            fontSize: FontSize.Normal.toString() + 'rem',
        }}
        suffix="円"
    />
)

export default App
