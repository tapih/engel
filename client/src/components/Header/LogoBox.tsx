import * as React from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'

import { FontSizeHeading } from 'constants/styles'
import logo from 'assets/img/logo.png'

import { ROUTE_HOME } from 'constants/routes'

const App = () => (
    <Container>
        <Link to={ROUTE_HOME}>
            <Logo src={logo} alt="Logo" />
            <LogoText>Engel</LogoText>
        </Link>
    </Container>
)

const Container = styled.div`
    display: flex;
`

const Logo = styled.img`
    width: 6rem;
    height: 6rem;
    margin-right: 1rem;
`

const LogoText = styled.span`
    color: white;
    font-size: ${FontSizeHeading.Large}rem;
    font-weight: 500;
    letter-spacing: 1px;
`

export default App
