import { css } from 'styled-components'

export const Ellipsis = css`
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
`

export const MoveUpOnHover = css`
    box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
    transition: transform 0.2s;
    &:hover {
        box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
        transform: translateY(-2px);
    }
`
