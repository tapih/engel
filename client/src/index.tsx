import * as React from 'react'
import * as ReactDOM from 'react-dom'
import { createStore } from 'redux'
import { Provider } from 'react-redux'

import reducer from './modules'
import Root from './routes'
import registerServiceWorker from './registerServiceWorker'

import 'assets/less/index.less'

const store = createStore(reducer)

ReactDOM.render(
    <Provider store={store}>
        <Root />
    </Provider>,
    document.getElementById('root') as HTMLElement,
)
registerServiceWorker()
