import { Action } from 'redux'

enum ActionType {
    EnableLoading,
    DisableLoading,
}

export interface IState {
    isLoading: boolean
}

interface IAction extends Action {
    payload: Partial<IState>
    type: ActionType
}

export type IEnableLoading = () => IAction
export const enableLoading = () => ({
    payload: {},
    type: ActionType.EnableLoading,
})

export type IDisableLoading = () => IAction
export const DisableLoading = () => ({
    payload: {},
    type: ActionType.DisableLoading,
})

const reducer = (
    state: IState = { isLoading: false },
    action: IAction,
): IState => {
    switch (action.type) {
        case ActionType.EnableLoading:
            return {
                ...state,
                isLoading: true,
            }
        case ActionType.DisableLoading:
            return {
                ...state,
                isLoading: false,
            }
        default:
            return state
    }
}

export default reducer
