import { combineReducers } from 'redux'

import pageReducer, { IState as IPageState } from './page'

export interface IReduxState {
    page: IPageState
}

const reducer = combineReducers({
    page: pageReducer,
})

export default reducer
