import * as React from 'react'
import { Layout, BackTop } from 'antd'

import CardGrid from 'components/BookCardGrid'
import Header from 'components/Header'

const { Content, Footer } = Layout

class App extends React.Component {
    public render() {
        return (
            <Layout>
                <Header />
                <Content style={{ padding: '0 50px' }}>
                    <CardGrid
                        numPerRow={5}
                        innerWidth={32}
                        innerHeight={43}
                        gutter={1}
                    />
                </Content>
                <Footer style={{ textAlign: 'center' }}>
                    &copy; 2019 Created by H.Muraoka
                </Footer>
                <BackTop style={{ bottom: '5rem', right: '5rem' }} />
            </Layout>
        )
    }
}

export default App
