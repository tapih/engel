import * as React from 'react'
import { Layout, Form, Button, Input } from 'antd'

import Header from 'components/Header'

class App extends React.Component {
    public render() {
        return (
            <Layout>
                <Header />
                <Form>
                    <Form.Item>
                        <Input placeholder="タイトル" />
                    </Form.Item>
                    <Form.Item>
                        <Input placeholder="説明" />
                    </Form.Item>
                    <Form.Item>
                        <Button type="primary">送信</Button>
                    </Form.Item>
                </Form>
            </Layout>
        )
    }
}

export default App
