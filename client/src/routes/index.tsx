import * as React from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import { Spin } from 'antd'
import { connect } from 'react-redux'

import Home from './Home'
import BookNew from './BookNew'
import { IReduxState } from 'modules'

import * as RoutePath from 'constants/routes'

interface IStateProps {
    isLoading: boolean
}

class App extends React.Component<IStateProps> {
    public render() {
        return (
            <React.Fragment>
                {this.props.isLoading ? (
                    <Spin />
                ) : (
                    <Router>
                        <Switch>
                            <Route
                                exact
                                path={RoutePath.ROUTE_HOME}
                                component={Home}
                            />
                            <Route
                                path={RoutePath.ROUTE_BOOK_NEW}
                                component={BookNew}
                            />
                        </Switch>
                    </Router>
                )}
            </React.Fragment>
        )
    }
}

const mapStateToProps = (state: IReduxState) => ({
    isLoading: state.page.isLoading,
})

export default connect(mapStateToProps)(App)
